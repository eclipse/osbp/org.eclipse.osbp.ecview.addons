/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroupAssigment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Vertical Component Group Assigment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiVerticalComponentGroupAssigmentTest extends TestCase {

	/**
	 * The fixture for this Ui Vertical Component Group Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVerticalComponentGroupAssigment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiVerticalComponentGroupAssigmentTest.class);
	}

	/**
	 * Constructs a new Ui Vertical Component Group Assigment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiVerticalComponentGroupAssigmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Vertical Component Group Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiVerticalComponentGroupAssigment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Vertical Component Group Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVerticalComponentGroupAssigment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiVerticalComponentGroupAssigment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiVerticalComponentGroupAssigmentTest
