/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiTypeProvider;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Type Provider</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiTypeProviderTest extends TestCase {

	/**
	 * The fixture for this Ui Type Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypeProvider fixture = null;

	/**
	 * Constructs a new Ui Type Provider test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiTypeProviderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Type Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiTypeProvider fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Type Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypeProvider getFixture() {
		return fixture;
	}

} //UiTypeProviderTest
