/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiColumnsAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Columns Assignment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiColumnsAssignmentTest extends UiVisibilityProcessableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiColumnsAssignmentTest.class);
	}

	/**
	 * Constructs a new Ui Columns Assignment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiColumnsAssignmentTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Columns Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiColumnsAssignment getFixture() {
		return (UiColumnsAssignment)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiColumnsAssignment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiColumnsAssignmentTest
