/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiXbaseVisibilityRule;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Xbase Visibility Rule</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiXbaseVisibilityRuleTest extends UiVisibilityRuleTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiXbaseVisibilityRuleTest.class);
	}

	/**
	 * Constructs a new Ui Xbase Visibility Rule test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiXbaseVisibilityRuleTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Xbase Visibility Rule test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiXbaseVisibilityRule getFixture() {
		return (UiXbaseVisibilityRule)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiXbaseVisibilityRule());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiXbaseVisibilityRuleTest
