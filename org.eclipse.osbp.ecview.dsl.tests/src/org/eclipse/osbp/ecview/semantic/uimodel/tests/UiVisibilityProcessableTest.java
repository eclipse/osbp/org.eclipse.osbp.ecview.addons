/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Visibility Processable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiVisibilityProcessableTest extends TestCase {

	/**
	 * The fixture for this Ui Visibility Processable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityProcessable fixture = null;

	/**
	 * Constructs a new Ui Visibility Processable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiVisibilityProcessableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Visibility Processable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiVisibilityProcessable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Visibility Processable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityProcessable getFixture() {
		return fixture;
	}

} //UiVisibilityProcessableTest
