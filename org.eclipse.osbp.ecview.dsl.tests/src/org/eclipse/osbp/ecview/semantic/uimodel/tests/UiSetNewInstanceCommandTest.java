/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSetNewInstanceCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Set New Instance Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiSetNewInstanceCommandTest extends TestCase {

	/**
	 * The fixture for this Ui Set New Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSetNewInstanceCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiSetNewInstanceCommandTest.class);
	}

	/**
	 * Constructs a new Ui Set New Instance Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSetNewInstanceCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Set New Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiSetNewInstanceCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Set New Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSetNewInstanceCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiSetNewInstanceCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiSetNewInstanceCommandTest
