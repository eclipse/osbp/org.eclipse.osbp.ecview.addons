/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSplitpanelAssigment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Splitpanel Assigment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiSplitpanelAssigmentTest extends TestCase {

	/**
	 * The fixture for this Ui Splitpanel Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSplitpanelAssigment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiSplitpanelAssigmentTest.class);
	}

	/**
	 * Constructs a new Ui Splitpanel Assigment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSplitpanelAssigmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Splitpanel Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiSplitpanelAssigment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Splitpanel Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSplitpanelAssigment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiSplitpanelAssigment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiSplitpanelAssigmentTest
