/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiRemoveFromTableCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Remove From Table Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiRemoveFromTableCommandTest extends TestCase {

	/**
	 * The fixture for this Ui Remove From Table Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRemoveFromTableCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiRemoveFromTableCommandTest.class);
	}

	/**
	 * Constructs a new Ui Remove From Table Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiRemoveFromTableCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Remove From Table Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiRemoveFromTableCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Remove From Table Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRemoveFromTableCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiRemoveFromTableCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiRemoveFromTableCommandTest
