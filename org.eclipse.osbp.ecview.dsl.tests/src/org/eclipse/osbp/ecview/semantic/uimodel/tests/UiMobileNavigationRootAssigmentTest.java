/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRootAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Navigation Root Assigment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiMobileNavigationRootAssigmentTest extends TestCase {

	/**
	 * The fixture for this Ui Mobile Navigation Root Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiMobileNavigationRootAssigment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiMobileNavigationRootAssigmentTest.class);
	}

	/**
	 * Constructs a new Ui Mobile Navigation Root Assigment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileNavigationRootAssigmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Mobile Navigation Root Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiMobileNavigationRootAssigment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Mobile Navigation Root Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiMobileNavigationRootAssigment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiMobileNavigationRootAssigment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiMobileNavigationRootAssigmentTest
