/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiRootElements;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Root Elements</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiRootElementsTest extends TestCase {

	/**
	 * The fixture for this Ui Root Elements test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRootElements fixture = null;

	/**
	 * Constructs a new Ui Root Elements test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiRootElementsTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Root Elements test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiRootElements fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Root Elements test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRootElements getFixture() {
		return fixture;
	}

} //UiRootElementsTest
