/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Search Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiSearchFieldTest extends UiVisibilityProcessableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiSearchFieldTest.class);
	}

	/**
	 * Constructs a new Ui Search Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSearchFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Search Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiSearchField getFixture() {
		return (UiSearchField)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiSearchField());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiSearchFieldTest
