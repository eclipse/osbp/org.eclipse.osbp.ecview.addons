/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorDef;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Visibility Processor Def</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiVisibilityProcessorDefTest extends TestCase {

	/**
	 * The fixture for this Ui Visibility Processor Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityProcessorDef fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiVisibilityProcessorDefTest.class);
	}

	/**
	 * Constructs a new Ui Visibility Processor Def test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiVisibilityProcessorDefTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Visibility Processor Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiVisibilityProcessorDef fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Visibility Processor Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityProcessorDef getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiVisibilityProcessorDef());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiVisibilityProcessorDefTest
