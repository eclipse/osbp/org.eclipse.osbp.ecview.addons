/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileLayout;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Layout</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiMobileLayoutTest extends UiVisibilityProcessableTest {

	/**
	 * Constructs a new Ui Mobile Layout test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileLayoutTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Mobile Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiMobileLayout getFixture() {
		return (UiMobileLayout)fixture;
	}

} //UiMobileLayoutTest
