/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiNamedElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Named Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiNamedElementTest extends TestCase {

	/**
	 * The fixture for this Ui Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiNamedElement fixture = null;

	/**
	 * Constructs a new Ui Named Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiNamedElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiNamedElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiNamedElement getFixture() {
		return fixture;
	}

} //UiNamedElementTest
