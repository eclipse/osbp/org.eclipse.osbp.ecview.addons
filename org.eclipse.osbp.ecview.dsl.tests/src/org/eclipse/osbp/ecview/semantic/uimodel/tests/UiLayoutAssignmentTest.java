/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiLayoutAssignment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Layout Assignment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiLayoutAssignmentTest extends TestCase {

	/**
	 * The fixture for this Ui Layout Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiLayoutAssignment fixture = null;

	/**
	 * Constructs a new Ui Layout Assignment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiLayoutAssignmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Layout Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiLayoutAssignment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Layout Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiLayoutAssignment getFixture() {
		return fixture;
	}

} //UiLayoutAssignmentTest
