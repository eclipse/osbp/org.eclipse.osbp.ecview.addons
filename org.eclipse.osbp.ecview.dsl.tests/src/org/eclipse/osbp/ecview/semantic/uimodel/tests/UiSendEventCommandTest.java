/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSendEventCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Send Event Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiSendEventCommandTest extends TestCase {

	/**
	 * The fixture for this Ui Send Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSendEventCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiSendEventCommandTest.class);
	}

	/**
	 * Constructs a new Ui Send Event Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSendEventCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Send Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiSendEventCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Send Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSendEventCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiSendEventCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiSendEventCommandTest
