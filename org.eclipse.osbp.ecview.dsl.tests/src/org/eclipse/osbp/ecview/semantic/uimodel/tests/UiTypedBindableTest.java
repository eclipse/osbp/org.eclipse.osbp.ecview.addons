/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiTypedBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Typed Bindable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiTypedBindableTest extends TestCase {

	/**
	 * The fixture for this Ui Typed Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypedBindable fixture = null;

	/**
	 * Constructs a new Ui Typed Bindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiTypedBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Typed Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiTypedBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Typed Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypedBindable getFixture() {
		return fixture;
	}

} //UiTypedBindableTest
