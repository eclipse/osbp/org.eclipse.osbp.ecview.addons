/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Prefix To Mask Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiPrefixToMaskMapEntryTest extends TestCase {

	/**
	 * The fixture for this Ui Prefix To Mask Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<String, String> fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiPrefixToMaskMapEntryTest.class);
	}

	/**
	 * Constructs a new Ui Prefix To Mask Map Entry test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiPrefixToMaskMapEntryTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Prefix To Mask Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Map.Entry<String, String> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Prefix To Mask Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<String, String> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void setUp() throws Exception {
		setFixture((Map.Entry<String, String>)UiModelFactory.eINSTANCE.create(UiModelPackage.Literals.UI_PREFIX_TO_MASK_MAP_ENTRY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiPrefixToMaskMapEntryTest
