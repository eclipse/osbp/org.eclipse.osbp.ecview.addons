/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiI18nInfoable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui I1 8n Infoable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiI18nInfoableTest extends TestCase {

	/**
	 * The fixture for this Ui I1 8n Infoable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiI18nInfoable fixture = null;

	/**
	 * Constructs a new Ui I1 8n Infoable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiI18nInfoableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui I1 8n Infoable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiI18nInfoable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui I1 8n Infoable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiI18nInfoable getFixture() {
		return fixture;
	}

} //UiI18nInfoableTest
