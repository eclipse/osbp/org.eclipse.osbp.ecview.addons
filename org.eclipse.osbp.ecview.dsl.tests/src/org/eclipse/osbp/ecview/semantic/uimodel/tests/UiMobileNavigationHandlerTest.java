/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationHandler;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Navigation Handler</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiMobileNavigationHandlerTest extends TestCase {

	/**
	 * The fixture for this Ui Mobile Navigation Handler test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiMobileNavigationHandler fixture = null;

	/**
	 * Constructs a new Ui Mobile Navigation Handler test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileNavigationHandlerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Mobile Navigation Handler test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiMobileNavigationHandler fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Mobile Navigation Handler test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiMobileNavigationHandler getFixture() {
		return fixture;
	}

} //UiMobileNavigationHandlerTest
