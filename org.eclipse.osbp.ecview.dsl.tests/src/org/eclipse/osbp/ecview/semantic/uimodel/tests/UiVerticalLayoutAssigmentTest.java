/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayoutAssigment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Vertical Layout Assigment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiVerticalLayoutAssigmentTest extends TestCase {

	/**
	 * The fixture for this Ui Vertical Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVerticalLayoutAssigment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiVerticalLayoutAssigmentTest.class);
	}

	/**
	 * Constructs a new Ui Vertical Layout Assigment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiVerticalLayoutAssigmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Vertical Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiVerticalLayoutAssigment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Vertical Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVerticalLayoutAssigment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiVerticalLayoutAssigment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiVerticalLayoutAssigmentTest
