/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationButton;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Navigation Button</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiMobileNavigationButtonTest extends UiMobileActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiMobileNavigationButtonTest.class);
	}

	/**
	 * Constructs a new Ui Mobile Navigation Button test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileNavigationButtonTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Mobile Navigation Button test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiMobileNavigationButton getFixture() {
		return (UiMobileNavigationButton)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiMobileNavigationButton());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiMobileNavigationButtonTest
