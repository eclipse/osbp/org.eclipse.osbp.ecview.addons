/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiOpenDialogCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Open Dialog Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiOpenDialogCommandTest extends TestCase {

	/**
	 * The fixture for this Ui Open Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiOpenDialogCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiOpenDialogCommandTest.class);
	}

	/**
	 * Constructs a new Ui Open Dialog Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiOpenDialogCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Open Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiOpenDialogCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Open Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiOpenDialogCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiOpenDialogCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiOpenDialogCommandTest
