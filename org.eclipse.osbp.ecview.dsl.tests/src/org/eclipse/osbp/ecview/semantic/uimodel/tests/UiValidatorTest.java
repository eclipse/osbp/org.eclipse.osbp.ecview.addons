/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiValidatorTest extends TestCase {

	/**
	 * The fixture for this Ui Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiValidator fixture = null;

	/**
	 * Constructs a new Ui Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiValidatorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiValidator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiValidator getFixture() {
		return fixture;
	}

} //UiValidatorTest
