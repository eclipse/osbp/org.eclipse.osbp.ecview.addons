/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchWithDialogCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Search With Dialog Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiSearchWithDialogCommandTest extends TestCase {

	/**
	 * The fixture for this Ui Search With Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSearchWithDialogCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiSearchWithDialogCommandTest.class);
	}

	/**
	 * Constructs a new Ui Search With Dialog Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSearchWithDialogCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Search With Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiSearchWithDialogCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Search With Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSearchWithDialogCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiSearchWithDialogCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiSearchWithDialogCommandTest
