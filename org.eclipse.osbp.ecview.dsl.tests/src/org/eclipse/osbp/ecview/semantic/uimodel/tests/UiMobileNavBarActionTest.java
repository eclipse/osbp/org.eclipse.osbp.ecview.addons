/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavBarAction;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Nav Bar Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiMobileNavBarActionTest extends UiMobileActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiMobileNavBarActionTest.class);
	}

	/**
	 * Constructs a new Ui Mobile Nav Bar Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileNavBarActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Mobile Nav Bar Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiMobileNavBarAction getFixture() {
		return (UiMobileNavBarAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiMobileNavBarAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiMobileNavBarActionTest
