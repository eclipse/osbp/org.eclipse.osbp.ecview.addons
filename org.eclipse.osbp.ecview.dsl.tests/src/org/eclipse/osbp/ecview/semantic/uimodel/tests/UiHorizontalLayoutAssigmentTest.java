/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayoutAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Horizontal Layout Assigment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiHorizontalLayoutAssigmentTest extends TestCase {

	/**
	 * The fixture for this Ui Horizontal Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiHorizontalLayoutAssigment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiHorizontalLayoutAssigmentTest.class);
	}

	/**
	 * Constructs a new Ui Horizontal Layout Assigment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiHorizontalLayoutAssigmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Horizontal Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiHorizontalLayoutAssigment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Horizontal Layout Assigment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiHorizontalLayoutAssigment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiHorizontalLayoutAssigment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiHorizontalLayoutAssigmentTest
