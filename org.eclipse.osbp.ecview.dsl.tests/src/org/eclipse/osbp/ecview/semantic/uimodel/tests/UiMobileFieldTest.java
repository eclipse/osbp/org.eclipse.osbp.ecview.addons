/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiMobileFieldTest extends UiVisibilityProcessableTest {

	/**
	 * Constructs a new Ui Mobile Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Mobile Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiMobileField getFixture() {
		return (UiMobileField)fixture;
	}

} //UiMobileFieldTest
