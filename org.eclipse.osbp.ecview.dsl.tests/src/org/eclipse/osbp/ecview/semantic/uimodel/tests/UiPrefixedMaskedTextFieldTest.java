/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiPrefixedMaskedTextField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Prefixed Masked Text Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiPrefixedMaskedTextFieldTest extends UiVisibilityProcessableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiPrefixedMaskedTextFieldTest.class);
	}

	/**
	 * Constructs a new Ui Prefixed Masked Text Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiPrefixedMaskedTextFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Prefixed Masked Text Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiPrefixedMaskedTextField getFixture() {
		return (UiPrefixedMaskedTextField)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiPrefixedMaskedTextField());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiPrefixedMaskedTextFieldTest
