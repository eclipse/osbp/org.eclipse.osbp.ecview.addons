/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileEmbeddable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Mobile Embeddable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiMobileEmbeddableTest extends UiVisibilityProcessableTest {

	/**
	 * Constructs a new Ui Mobile Embeddable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiMobileEmbeddableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Mobile Embeddable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiMobileEmbeddable getFixture() {
		return (UiMobileEmbeddable)fixture;
	}

} //UiMobileEmbeddableTest
