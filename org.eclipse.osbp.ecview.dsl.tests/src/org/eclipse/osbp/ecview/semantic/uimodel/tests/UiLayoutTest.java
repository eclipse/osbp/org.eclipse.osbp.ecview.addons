/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import org.eclipse.osbp.ecview.semantic.uimodel.UiLayout;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Layout</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiLayoutTest extends UiVisibilityProcessableTest {

	/**
	 * Constructs a new Ui Layout test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiLayoutTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiLayout getFixture() {
		return (UiLayout)fixture;
	}

} //UiLayoutTest
