/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiProgressBar;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Progress Bar</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiProgressBarTest extends UiVisibilityProcessableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiProgressBarTest.class);
	}

	/**
	 * Constructs a new Ui Progress Bar test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiProgressBarTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Progress Bar test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiProgressBar getFixture() {
		return (UiProgressBar)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiProgressBar());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiProgressBarTest
