/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiRawBindableProvider;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Raw Bindable Provider</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiRawBindableProviderTest extends TestCase {

	/**
	 * The fixture for this Ui Raw Bindable Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRawBindableProvider fixture = null;

	/**
	 * Constructs a new Ui Raw Bindable Provider test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiRawBindableProviderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Raw Bindable Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiRawBindableProvider fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Raw Bindable Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRawBindableProvider getFixture() {
		return fixture;
	}

} //UiRawBindableProviderTest
