/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiList;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui List</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiListTest extends UiVisibilityProcessableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiListTest.class);
	}

	/**
	 * Constructs a new Ui List test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiListTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui List test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiList getFixture() {
		return (UiList)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiList());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiListTest
