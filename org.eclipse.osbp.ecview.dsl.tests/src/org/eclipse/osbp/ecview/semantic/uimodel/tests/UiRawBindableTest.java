/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiRawBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Raw Bindable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiRawBindableTest extends TestCase {

	/**
	 * The fixture for this Ui Raw Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRawBindable fixture = null;

	/**
	 * Constructs a new Ui Raw Bindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiRawBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Raw Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiRawBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Raw Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiRawBindable getFixture() {
		return fixture;
	}

} //UiRawBindableTest
