/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityRule;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Visibility Rule</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiVisibilityRuleTest extends TestCase {

	/**
	 * The fixture for this Ui Visibility Rule test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityRule fixture = null;

	/**
	 * Constructs a new Ui Visibility Rule test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiVisibilityRuleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Visibility Rule test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiVisibilityRule fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Visibility Rule test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiVisibilityRule getFixture() {
		return fixture;
	}

} //UiVisibilityRuleTest
