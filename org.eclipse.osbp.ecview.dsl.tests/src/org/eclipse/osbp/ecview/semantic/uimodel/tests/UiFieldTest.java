/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import org.eclipse.osbp.ecview.semantic.uimodel.UiField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiFieldTest extends UiVisibilityProcessableTest {

	/**
	 * Constructs a new Ui Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ui Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UiField getFixture() {
		return (UiField)fixture;
	}

} //UiFieldTest
