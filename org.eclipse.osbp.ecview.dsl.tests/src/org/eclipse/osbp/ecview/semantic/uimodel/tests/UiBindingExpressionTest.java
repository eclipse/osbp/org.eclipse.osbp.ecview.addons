/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Binding Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UiBindingExpressionTest extends TestCase {

	/**
	 * The fixture for this Ui Binding Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiBindingExpression fixture = null;

	/**
	 * Constructs a new Ui Binding Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiBindingExpressionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Binding Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiBindingExpression fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Binding Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiBindingExpression getFixture() {
		return fixture;
	}

} //UiBindingExpressionTest
