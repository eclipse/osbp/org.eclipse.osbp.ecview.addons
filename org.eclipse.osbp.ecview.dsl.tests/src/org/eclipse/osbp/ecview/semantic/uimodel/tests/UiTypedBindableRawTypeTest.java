/**
 */
package org.eclipse.osbp.ecview.semantic.uimodel.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTypedBindableRawType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ui Typed Bindable Raw Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UiTypedBindableRawTypeTest extends TestCase {

	/**
	 * The fixture for this Ui Typed Bindable Raw Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypedBindableRawType fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UiTypedBindableRawTypeTest.class);
	}

	/**
	 * Constructs a new Ui Typed Bindable Raw Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiTypedBindableRawTypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ui Typed Bindable Raw Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UiTypedBindableRawType fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ui Typed Bindable Raw Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiTypedBindableRawType getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiModelFactory.eINSTANCE.createUiTypedBindableRawType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UiTypedBindableRawTypeTest
