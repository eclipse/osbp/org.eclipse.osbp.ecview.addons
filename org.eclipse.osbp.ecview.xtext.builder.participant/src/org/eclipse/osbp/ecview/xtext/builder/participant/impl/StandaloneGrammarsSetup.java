/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.xtext.builder.participant.impl;

import org.eclipse.osbp.ecview.dsl.UIGrammarBundleSpaceStandaloneSetup;
import org.eclipse.osbp.ecview.uisemantics.UISemanticsGrammarBundleSpaceStandaloneSetup;

/**
 * A helper class to setup the Xtext grammars properly.
 */
@SuppressWarnings("restriction")
public class StandaloneGrammarsSetup {

	public static void setup() {
		UISemanticsGrammarBundleSpaceStandaloneSetup.doSetup();
		UIGrammarBundleSpaceStandaloneSetup.doSetup();
	}

}
