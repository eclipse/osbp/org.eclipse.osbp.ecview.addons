/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.xtext.builder.participant;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.model.core.YView;

public interface IECViewAddonsMetadataService {

	/**
	 * Returns the YView for the given viewId.
	 * 
	 * @param viewId
	 * @return
	 */
	YView getViewMetadata(String viewId);

	/**
	 * Returns a filtered list with all names of IDEViews.
	 * <p>
	 * If package <code>null</code> is passed and
	 * <code>includeChildren == true</code>, then all views are returned.<br>
	 * If package <code>null</code> is passed and <code>includeChildren ==
	 * false</code>, then only views of the most common packages are returned.
	 * What means, if a view is found in "org.eclipse.osbp.sample" then views from
	 * "org.eclipse.osbp.sample.general" are skipped.
	 * 
	 * @param packageName
	 *            - the package name the views need to be located in.
	 * @param includeChildren
	 *            - if true, then also child packages are queried
	 * @return
	 */
	List<String> getIDEViewNames(String packageName, boolean includeChildren);

	/**
	 * Returns a filtered list with all names of MobileViews.
	 * <p>
	 * If package <code>null</code> is passed and
	 * <code>includeChildren == true</code>, then all views are returned.<br>
	 * If package <code>null</code> is passed and <code>includeChildren ==
	 * false</code>, then only views of the most common packages are returned.
	 * What means, if a view is found in "org.eclipse.osbp.sample" then views from
	 * "org.eclipse.osbp.sample.general" are skipped.
	 * 
	 * @param packageName
	 *            - the package name the views need to be located in.
	 * @param includeChildren
	 *            - if true, then also child packages are queried
	 * @return
	 */
	List<String> getMobileViewNames(String packageName, boolean includeChildren);

}
