/*******************************************************************************
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *   Jan Koehnlein - Initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.ecview.vaadin.ide.preview.parts;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.osbp.ecview.core.common.model.core.YDeviceType;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.dsl.derivedstate.UiModelGrammarUtil;
import org.eclipse.osbp.ecview.semantic.uimodel.UiIDEView;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel;
import org.eclipse.osbp.ecview.semantic.uimodel.UiView;
import org.eclipse.osbp.ecview.vaadin.ide.preview.PreviewActivator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.IXtextModelListener;
import org.eclipse.xtext.util.ITextRegion;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Singleton;

/**
 */
@SuppressWarnings("restriction")
@Singleton
public class ECViewVaadinSynchronizer implements IPartListener,
		IXtextModelListener, ISelectionChangedListener {

	private IXtextDocument lastActiveDocument;
	private XtextEditor lastActiveEditor;

	private XtextSourceViewer viewer;

	public void start(IWorkbenchPartSite site) {
		updateView(site.getPage().getActiveEditor());
		site.getWorkbenchWindow().getPartService().addPartListener(this);
		PreviewActivator.getIDEPreviewHandler().setSynchronizer(this);
	}

	public void stop(IWorkbenchPartSite site) {
		site.getWorkbenchWindow().getPartService().removePartListener(this);
		lastActiveDocument = null;
		lastActiveEditor = null;
		PreviewActivator.getIDEPreviewHandler().setSynchronizer(null);
	}

	public void selectInXtextEditor(EObject element) {
		EObject grammarElement = UiModelGrammarUtil
				.getUiGrammarElement(element);
		if (grammarElement != null) {
			CompositeNodeWithSemanticElement node = (CompositeNodeWithSemanticElement) NodeModelUtils
					.getNode(grammarElement);
			if (node != null) {
				selectInXtextEditor(node.getTextRegion());
			}
		}
	}

	public void selectInXtextEditor(final ITextRegion region) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if (region != null && region != ITextRegion.EMPTY_REGION) {
					int offset = region.getOffset();
					int length = region.getLength();
					viewer.setRangeIndication(offset, length, true);
					viewer.revealRange(offset, length);
					viewer.setSelectedRange(offset, length);
				}
			}
		});
	}

	public void partActivated(IWorkbenchPart part) {
		updateView(part);
	}

	private void updateView(IWorkbenchPart part) {
		if (part instanceof XtextEditor) {
			XtextEditor xtextEditor = (XtextEditor) part;
			IXtextDocument xtextDocument = xtextEditor.getDocument();
			if (xtextDocument != lastActiveDocument) {
				if (lastActiveDocument != null) {
					lastActiveDocument.removeModelListener(this);
					viewer.removePostSelectionChangedListener(this);
				}

				lastActiveDocument = xtextDocument;
				lastActiveEditor = xtextEditor;
				viewer = (XtextSourceViewer) lastActiveEditor
						.getInternalSourceViewer();
				lastActiveDocument.addModelListener(this);
				viewer.addPostSelectionChangedListener(this);
				lastActiveDocument
						.readOnly(new IUnitOfWork<Boolean, XtextResource>() {
							@Override
							public Boolean exec(XtextResource state)
									throws Exception {
								modelChanged(state);
								return true;
							}
						});
			}
		}
	}

	public void partBroughtToTop(IWorkbenchPart part) {
	}

	public void partClosed(IWorkbenchPart part) {
	}

	public void partDeactivated(IWorkbenchPart part) {
	}

	public void partOpened(IWorkbenchPart part) {
	}

	public void modelChanged(XtextResource resource) {
//		if (resource == null) {
//			return;
//		}
//		if (resource.getContents().size() < 2) {
//			return;
//		}
//		for (EObject e : resource.getContents()) {
//			if (e instanceof YView) {
//				YView view = (YView) e;
//				if (view.getDeviceType() != YDeviceType.MOBILE) {
//					PreviewActivator.getIDEPreviewHandler()
//							.setActiveViewFromXtextEditor(view);
//				}
//				break;
//			}
//		}
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		final ISelection selection = event.getSelection();
		if (selection instanceof ITextSelection) {
			final ITextSelection textSelection = (ITextSelection) selection;
			if (lastActiveDocument != null) {
				lastActiveDocument
						.readOnly(new IUnitOfWork<Boolean, XtextResource>() {
							@Override
							public Boolean exec(XtextResource state)
									throws Exception {
								YView yView = findYView(textSelection, state);

								if (yView != null) {
									PreviewActivator
											.getIDEPreviewHandler()
											.setActiveViewFromXtextEditor(yView);
								}
								return true;
							}
						});
			}
		}
	}

	protected YView findYView(String fqn, XtextResource state) {
		for (EObject content : state.getContents()) {
			if (content instanceof YView) {
				YView view = (YView) content;
				if (view.getName().equals(fqn)) {
					return view;
				}
			}
		}
		return null;
	}

	protected UiView findView(EObject eObject) {
		if (eObject == null) {
			return null;
		}
		if (eObject instanceof UiView) {
			return (UiView) eObject;
		}
		return findView(eObject.eContainer());
	}

	private YView findYView(final ITextSelection textSelection,
			XtextResource state) {
		YView yView = null;
		EObjectAtOffsetHelper eObjectAtOffsetHelper = PreviewActivator
				.getDefault().getInjector()
				.getInstance(EObjectAtOffsetHelper.class);
		EObject eObject = eObjectAtOffsetHelper.resolveElementAt(state,
				textSelection.getOffset());
		UiView view = findView(eObject);
		if (view instanceof UiIDEView) {
			UiIDEView ideView = (UiIDEView) view;
			UiModel model = (UiModel) ideView.eContainer();
			String fqn = model.getPackageName() + "." + ideView.getName();

			yView = findYView(fqn, state);
		}
		return yView;
	}
}
