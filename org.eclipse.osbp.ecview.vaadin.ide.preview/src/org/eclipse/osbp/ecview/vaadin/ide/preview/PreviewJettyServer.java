/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.ecview.vaadin.ide.preview;

import java.io.File;

import org.eclipse.osbp.ecview.vaadin.ide.preview.jetty.PreviewJettyManager;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

@Component(immediate = true)
public class PreviewJettyServer {

	private PreviewJettyManager jettyManager;

	@Activate
	void activate(ComponentContext context) {
		File jettyWorkDir = new File(
				context.getBundleContext().getDataFile(""), "vaadinpreview"); //$NON-NLS-1$ 
		jettyWorkDir.mkdir();
		jettyManager = new PreviewJettyManager(jettyWorkDir);
		jettyManager.start();
	}
	
	@Deactivate
	void deactivate() {
		try {
			jettyManager.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
