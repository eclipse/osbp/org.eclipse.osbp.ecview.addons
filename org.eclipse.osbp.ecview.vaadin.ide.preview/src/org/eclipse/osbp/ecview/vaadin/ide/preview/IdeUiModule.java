/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.vaadin.ide.preview;

import org.eclipse.osbp.ide.core.api.i18n.II18nRegistry;

import com.google.inject.Binder;
import com.google.inject.Module;

/**
 * Use this class to register components to be used within the IDE.
 */
public class IdeUiModule implements Module {

	@Override
	public void configure(Binder binder) {
		binder.bind(II18nRegistry.class).toProvider(
				org.eclipse.osbp.ide.core.ui.shared.Access.getII18nRegistry());
	}

}
