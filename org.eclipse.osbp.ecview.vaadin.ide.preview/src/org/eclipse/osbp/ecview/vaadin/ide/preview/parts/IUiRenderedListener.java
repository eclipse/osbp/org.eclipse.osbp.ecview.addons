
/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */



package org.eclipse.osbp.ecview.vaadin.ide.preview.parts;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;

public interface IUiRenderedListener {

	/**
	 * Notifies the listener, that a new view has been rendered.
	 * 
	 * @param context
	 */
	public void notifyNewViewRendered(IViewContext context);

}
