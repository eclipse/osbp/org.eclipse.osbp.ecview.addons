/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxViewCategory;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getViewSet <em>View Set</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getContent <em>Content</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getContentAlignment <em>Content Alignment</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getValidatorAssignments <em>Validator Assignments</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getViewCategory <em>View Category</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView()
 * @model
 * @generated
 */
public interface UiView extends UiContext, UiVisibilityProcessable, UiRawBindable, UiTypeProvider {

	/**
	 * Returns the value of the '<em><b>View Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>View Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View Set</em>' reference.
	 * @see #setViewSet(UiViewSet)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView_ViewSet()
	 * @model
	 * @generated
	 */
	UiViewSet getViewSet();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getViewSet <em>View Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>View Set</em>' reference.
	 * @see #getViewSet()
	 * @generated
	 */
	void setViewSet(UiViewSet value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' containment reference.
	 * @see #setContent(UiEmbeddable)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView_Content()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiEmbeddable getContent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getContent <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' containment reference.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(UiEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Content Alignment</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content Alignment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content Alignment</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment
	 * @see #setContentAlignment(UiAlignment)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView_ContentAlignment()
	 * @model
	 * @generated
	 */
	UiAlignment getContentAlignment();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getContentAlignment <em>Content Alignment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content Alignment</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment
	 * @see #getContentAlignment()
	 * @generated
	 */
	void setContentAlignment(UiAlignment value);

	/**
	 * Returns the value of the '<em><b>Validator Assignments</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.semantic.uimodel.UiValidatorAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validator Assignments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validator Assignments</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView_ValidatorAssignments()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<UiValidatorAssignment> getValidatorAssignments();

	/**
	 * Returns the value of the '<em><b>View Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>View Category</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View Category</em>' reference.
	 * @see #setViewCategory(UxViewCategory)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiView_ViewCategory()
	 * @model
	 * @generated
	 */
	UxViewCategory getViewCategory();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiView#getViewCategory <em>View Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>View Category</em>' reference.
	 * @see #getViewCategory()
	 * @generated
	 */
	void setViewCategory(UxViewCategory value);
} // UiView
