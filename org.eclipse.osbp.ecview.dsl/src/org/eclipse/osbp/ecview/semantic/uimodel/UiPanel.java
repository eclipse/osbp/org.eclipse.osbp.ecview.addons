/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Panel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiPanel#getContent <em>Content</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiPanel#getContentAlignment <em>Content Alignment</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiPanel()
 * @model
 * @generated
 */
public interface UiPanel extends UiLayout {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' containment reference.
	 * @see #setContent(UiEmbeddable)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiPanel_Content()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiEmbeddable getContent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiPanel#getContent <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' containment reference.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(UiEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Content Alignment</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content Alignment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content Alignment</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment
	 * @see #setContentAlignment(UiAlignment)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiPanel_ContentAlignment()
	 * @model
	 * @generated
	 */
	UiAlignment getContentAlignment();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiPanel#getContentAlignment <em>Content Alignment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content Alignment</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment
	 * @see #getContentAlignment()
	 * @generated
	 */
	void setContentAlignment(UiAlignment value);

} // UiPanel
