/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.xtext.common.types.JvmOperation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Suggest Text Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemCaptionProperty <em>Item Caption Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemFilterProperty <em>Item Filter Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemUUIDProperty <em>Item UUID Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#isAutoHidePopup <em>Auto Hide Popup</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSuggestTextField()
 * @model
 * @generated
 */
public interface UiSuggestTextField extends UiField, UiTypeProvider, UiMobileField, UiBeanServiceConsumer {

	/**
	 * Returns the value of the '<em><b>Item Caption Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item Caption Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item Caption Property</em>' reference.
	 * @see #setItemCaptionProperty(JvmOperation)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSuggestTextField_ItemCaptionProperty()
	 * @model
	 * @generated
	 */
	JvmOperation getItemCaptionProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemCaptionProperty <em>Item Caption Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item Caption Property</em>' reference.
	 * @see #getItemCaptionProperty()
	 * @generated
	 */
	void setItemCaptionProperty(JvmOperation value);

	/**
	 * Returns the value of the '<em><b>Item Filter Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item Filter Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item Filter Property</em>' reference.
	 * @see #setItemFilterProperty(JvmOperation)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSuggestTextField_ItemFilterProperty()
	 * @model
	 * @generated
	 */
	JvmOperation getItemFilterProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemFilterProperty <em>Item Filter Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item Filter Property</em>' reference.
	 * @see #getItemFilterProperty()
	 * @generated
	 */
	void setItemFilterProperty(JvmOperation value);

	/**
	 * Returns the value of the '<em><b>Item UUID Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item UUID Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item UUID Property</em>' reference.
	 * @see #setItemUUIDProperty(JvmOperation)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSuggestTextField_ItemUUIDProperty()
	 * @model
	 * @generated
	 */
	JvmOperation getItemUUIDProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#getItemUUIDProperty <em>Item UUID Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item UUID Property</em>' reference.
	 * @see #getItemUUIDProperty()
	 * @generated
	 */
	void setItemUUIDProperty(JvmOperation value);

	/**
	 * Returns the value of the '<em><b>Auto Hide Popup</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auto Hide Popup</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Hide Popup</em>' attribute.
	 * @see #setAutoHidePopup(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSuggestTextField_AutoHidePopup()
	 * @model
	 * @generated
	 */
	boolean isAutoHidePopup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField#isAutoHidePopup <em>Auto Hide Popup</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Hide Popup</em>' attribute.
	 * @see #isAutoHidePopup()
	 * @generated
	 */
	void setAutoHidePopup(boolean value);
} // UiSuggestTextField
