/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Visibility Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProperty#getBindableDef <em>Bindable Def</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProperty#getAssignmentExpression <em>Assignment Expression</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiVisibilityProperty()
 * @model
 * @generated
 */
public interface UiVisibilityProperty extends UiNamedElement {
	/**
	 * Returns the value of the '<em><b>Bindable Def</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindable Def</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindable Def</em>' containment reference.
	 * @see #setBindableDef(UiBindingExpression)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiVisibilityProperty_BindableDef()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiBindingExpression getBindableDef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProperty#getBindableDef <em>Bindable Def</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bindable Def</em>' containment reference.
	 * @see #getBindableDef()
	 * @generated
	 */
	void setBindableDef(UiBindingExpression value);

	/**
	 * Returns the value of the '<em><b>Assignment Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Expression</em>' containment reference.
	 * @see #setAssignmentExpression(XExpression)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiVisibilityProperty_AssignmentExpression()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	XExpression getAssignmentExpression();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProperty#getAssignmentExpression <em>Assignment Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Expression</em>' containment reference.
	 * @see #getAssignmentExpression()
	 * @generated
	 */
	void setAssignmentExpression(XExpression value);

} // UiVisibilityProperty
