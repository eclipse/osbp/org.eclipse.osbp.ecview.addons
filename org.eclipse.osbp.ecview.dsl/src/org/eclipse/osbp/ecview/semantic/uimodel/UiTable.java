/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.xtext.common.types.JvmOperation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getColumnAssignment <em>Column Assignment</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getSelectionType <em>Selection Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getItemImageProperty <em>Item Image Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getSortOrderAssignment <em>Sort Order Assignment</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#isScrollToBottom <em>Scroll To Bottom</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getPageLength <em>Page Length</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable()
 * @model
 * @generated
 */
public interface UiTable extends UiField, UiTypeProvider, UiMobileField, UiBeanServiceConsumer {
	/**
	 * Returns the value of the '<em><b>Column Assignment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column Assignment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column Assignment</em>' containment reference.
	 * @see #setColumnAssignment(UiColumnsAssignment)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_ColumnAssignment()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiColumnsAssignment getColumnAssignment();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getColumnAssignment <em>Column Assignment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column Assignment</em>' containment reference.
	 * @see #getColumnAssignment()
	 * @generated
	 */
	void setColumnAssignment(UiColumnsAssignment value);

	/**
	 * Returns the value of the '<em><b>Selection Type</b></em>' attribute.
	 * The default value is <code>"SINGLE"</code>.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.semantic.uimodel.UiSelectionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Type</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiSelectionType
	 * @see #setSelectionType(UiSelectionType)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_SelectionType()
	 * @model default="SINGLE" required="true"
	 * @generated
	 */
	UiSelectionType getSelectionType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getSelectionType <em>Selection Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Type</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiSelectionType
	 * @see #getSelectionType()
	 * @generated
	 */
	void setSelectionType(UiSelectionType value);

	/**
	 * Returns the value of the '<em><b>Item Image Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item Image Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item Image Property</em>' reference.
	 * @see #setItemImageProperty(JvmOperation)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_ItemImageProperty()
	 * @model
	 * @generated
	 */
	JvmOperation getItemImageProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getItemImageProperty <em>Item Image Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item Image Property</em>' reference.
	 * @see #getItemImageProperty()
	 * @generated
	 */
	void setItemImageProperty(JvmOperation value);

	/**
	 * Returns the value of the '<em><b>Sort Order Assignment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sort Order Assignment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sort Order Assignment</em>' containment reference.
	 * @see #setSortOrderAssignment(UiSortOrderAssignment)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_SortOrderAssignment()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiSortOrderAssignment getSortOrderAssignment();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getSortOrderAssignment <em>Sort Order Assignment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sort Order Assignment</em>' containment reference.
	 * @see #getSortOrderAssignment()
	 * @generated
	 */
	void setSortOrderAssignment(UiSortOrderAssignment value);

	/**
	 * Returns the value of the '<em><b>Scroll To Bottom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scroll To Bottom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scroll To Bottom</em>' attribute.
	 * @see #setScrollToBottom(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_ScrollToBottom()
	 * @model
	 * @generated
	 */
	boolean isScrollToBottom();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#isScrollToBottom <em>Scroll To Bottom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scroll To Bottom</em>' attribute.
	 * @see #isScrollToBottom()
	 * @generated
	 */
	void setScrollToBottom(boolean value);

	/**
	 * Returns the value of the '<em><b>Page Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Length</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Length</em>' attribute.
	 * @see #setPageLength(int)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiTable_PageLength()
	 * @model
	 * @generated
	 */
	int getPageLength();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiTable#getPageLength <em>Page Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Length</em>' attribute.
	 * @see #getPageLength()
	 * @generated
	 */
	void setPageLength(int value);

} // UiTable
