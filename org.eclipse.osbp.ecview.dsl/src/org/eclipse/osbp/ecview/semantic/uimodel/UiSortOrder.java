/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Sort Order</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSortOrder#getProperty <em>Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSortOrder#isAsc <em>Asc</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSortOrder()
 * @model
 * @generated
 */
public interface UiSortOrder extends UiField {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(UiNestedProperty)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSortOrder_Property()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiNestedProperty getProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSortOrder#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(UiNestedProperty value);

	/**
	 * Returns the value of the '<em><b>Asc</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asc</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asc</em>' attribute.
	 * @see #setAsc(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSortOrder_Asc()
	 * @model default="true"
	 * @generated
	 */
	boolean isAsc();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSortOrder#isAsc <em>Asc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asc</em>' attribute.
	 * @see #isAsc()
	 * @generated
	 */
	void setAsc(boolean value);

} // UiSortOrder
