
/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */


package org.eclipse.osbp.ecview.semantic.uimodel.util;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.semantic.uimodel.UiEmbeddable;
import org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction;
import org.eclipse.osbp.ecview.semantic.uimodel.UiNamedElement;
import org.eclipse.osbp.ecview.semantic.uimodel.UiView;

public class UiModelUtil {

	/**
	 * Returns the path for the current element up to the view will be concated
	 * by "." Elements with no name are skipped.
	 * 
	 * @return
	 */
	public static String getPathId(UiEmbeddable embeddable) {
		if (embeddable == null || embeddable.getName() == null
				|| embeddable.getName().equals("")) {
			// elements without name get a generated ID
			return null;
		}

		if (embeddable instanceof UiExposedAction) {
			return getPathId((UiExposedAction) embeddable);
		} else {
			List<EObject> elements = new LinkedList<EObject>();
			EObject current = embeddable;
			while (current != null) {
				elements.add(current);
				current = current.eContainer();
			}

			StringBuilder builder = new StringBuilder();
			for (int i = elements.size() - 1; i >= 0; i--) {
				EObject element = elements.get(i);
				if (element instanceof UiEmbeddable
						|| element instanceof UiView) {
					UiNamedElement temp = (UiNamedElement) element;
					if (temp.getName() != null && !temp.getName().equals("")) {
						if (builder.length() > 0) {
							builder.append(".");
						}
						builder.append(temp.getName());
					}
				}
			}
			return builder.toString();
		}
	}

	/**
	 * UiExposedActions use their unique id.
	 * 
	 * @return
	 */
	public static String getPathId(UiExposedAction action) {
		if (action.getActionReference() != null) {
			return action.getActionReference().getName();
		} else {
			return action.getActionID();
		}
	}
}
