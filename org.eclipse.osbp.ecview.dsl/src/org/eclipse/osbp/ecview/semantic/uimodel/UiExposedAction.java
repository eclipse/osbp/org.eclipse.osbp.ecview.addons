/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.osbp.ecview.semantic.uisemantics.UxAction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Exposed Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getActionReference <em>Action Reference</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getActionID <em>Action ID</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getIconName <em>Icon Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#isCheckDirty <em>Check Dirty</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getExternalCommandId <em>External Command Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction()
 * @model
 * @generated
 */
public interface UiExposedAction extends UiAction {
	/**
	 * Returns the value of the '<em><b>Action Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Reference</em>' reference.
	 * @see #setActionReference(UxAction)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction_ActionReference()
	 * @model
	 * @generated
	 */
	UxAction getActionReference();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getActionReference <em>Action Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Reference</em>' reference.
	 * @see #getActionReference()
	 * @generated
	 */
	void setActionReference(UxAction value);

	/**
	 * Returns the value of the '<em><b>Action ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action ID</em>' attribute.
	 * @see #setActionID(String)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction_ActionID()
	 * @model
	 * @generated
	 */
	String getActionID();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getActionID <em>Action ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action ID</em>' attribute.
	 * @see #getActionID()
	 * @generated
	 */
	void setActionID(String value);

	/**
	 * Returns the value of the '<em><b>Icon Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon Name</em>' attribute.
	 * @see #setIconName(String)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction_IconName()
	 * @model
	 * @generated
	 */
	String getIconName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getIconName <em>Icon Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon Name</em>' attribute.
	 * @see #getIconName()
	 * @generated
	 */
	void setIconName(String value);

	/**
	 * Returns the value of the '<em><b>Check Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Dirty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Dirty</em>' attribute.
	 * @see #setCheckDirty(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction_CheckDirty()
	 * @model
	 * @generated
	 */
	boolean isCheckDirty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#isCheckDirty <em>Check Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Dirty</em>' attribute.
	 * @see #isCheckDirty()
	 * @generated
	 */
	void setCheckDirty(boolean value);

	/**
	 * Returns the value of the '<em><b>External Command Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Command Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Command Id</em>' attribute.
	 * @see #setExternalCommandId(String)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiExposedAction_ExternalCommandId()
	 * @model
	 * @generated
	 */
	String getExternalCommandId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction#getExternalCommandId <em>External Command Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Command Id</em>' attribute.
	 * @see #getExternalCommandId()
	 * @generated
	 */
	void setExternalCommandId(String value);

} // UiExposedAction
