/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Search With Dialog Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSearchWithDialogCommand#getDialog <em>Dialog</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSearchWithDialogCommand()
 * @model
 * @generated
 */
public interface UiSearchWithDialogCommand extends UiCommand {
	/**
	 * Returns the value of the '<em><b>Dialog</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dialog</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dialog</em>' containment reference.
	 * @see #setDialog(UiSearchDialog)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiSearchWithDialogCommand_Dialog()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UiSearchDialog getDialog();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiSearchWithDialogCommand#getDialog <em>Dialog</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dialog</em>' containment reference.
	 * @see #getDialog()
	 * @generated
	 */
	void setDialog(UiSearchDialog value);

} // UiSearchWithDialogCommand
