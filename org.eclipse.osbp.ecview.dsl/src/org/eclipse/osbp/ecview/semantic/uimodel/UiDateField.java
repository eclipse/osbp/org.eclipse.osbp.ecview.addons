/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Date Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateField#getDateFormat <em>Date Format</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateField#getResolution <em>Resolution</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiDateField()
 * @model
 * @generated
 */
public interface UiDateField extends UiField, UiMobileField {

	/**
	 * Returns the value of the '<em><b>Date Format</b></em>' attribute.
	 * The default value is <code>"DATE"</code>.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateFormat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Format</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiDateFormat
	 * @see #setDateFormat(UiDateFormat)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiDateField_DateFormat()
	 * @model default="DATE"
	 * @generated
	 */
	UiDateFormat getDateFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateField#getDateFormat <em>Date Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Format</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiDateFormat
	 * @see #getDateFormat()
	 * @generated
	 */
	void setDateFormat(UiDateFormat value);

	/**
	 * Returns the value of the '<em><b>Resolution</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateTimeResolution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolution</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiDateTimeResolution
	 * @see #setResolution(UiDateTimeResolution)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiDateField_Resolution()
	 * @model default="UNDEFINED"
	 * @generated
	 */
	UiDateTimeResolution getResolution();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiDateField#getResolution <em>Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resolution</em>' attribute.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiDateTimeResolution
	 * @see #getResolution()
	 * @generated
	 */
	void setResolution(UiDateTimeResolution value);
} // UiDateField
