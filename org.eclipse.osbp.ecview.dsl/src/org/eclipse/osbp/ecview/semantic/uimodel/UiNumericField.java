/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Numeric Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField#isNoGrouping <em>No Grouping</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField#isNoMarkNegative <em>No Mark Negative</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiNumericField()
 * @model
 * @generated
 */
public interface UiNumericField extends UiField, UiMobileField {

	/**
	 * Returns the value of the '<em><b>No Grouping</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Grouping</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Grouping</em>' attribute.
	 * @see #setNoGrouping(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiNumericField_NoGrouping()
	 * @model default="false"
	 * @generated
	 */
	boolean isNoGrouping();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField#isNoGrouping <em>No Grouping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Grouping</em>' attribute.
	 * @see #isNoGrouping()
	 * @generated
	 */
	void setNoGrouping(boolean value);

	/**
	 * Returns the value of the '<em><b>No Mark Negative</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Mark Negative</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Mark Negative</em>' attribute.
	 * @see #setNoMarkNegative(boolean)
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiNumericField_NoMarkNegative()
	 * @model default="false"
	 * @generated
	 */
	boolean isNoMarkNegative();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField#isNoMarkNegative <em>No Mark Negative</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Mark Negative</em>' attribute.
	 * @see #isNoMarkNegative()
	 * @generated
	 */
	void setNoMarkNegative(boolean value);
} // UiNumericField
