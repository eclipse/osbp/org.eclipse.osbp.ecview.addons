/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ui Visibility Processable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessable#getProcessorAssignments <em>Processor Assignments</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiVisibilityProcessable()
 * @model abstract="true"
 * @generated
 */
public interface UiVisibilityProcessable extends EObject {
	/**
	 * Returns the value of the '<em><b>Processor Assignments</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor Assignments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor Assignments</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage#getUiVisibilityProcessable_ProcessorAssignments()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<UiVisibilityProcessorAssignment> getProcessorAssignments();

} // UiVisibilityProcessable
