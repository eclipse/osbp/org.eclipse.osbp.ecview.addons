/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.scope.jvmtype

import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel
import org.eclipse.osbp.xtext.oxtype.scoping.IScopingInfoProvider
import org.eclipse.osbp.xtext.oxtype.scoping.jvmtype.OXJvmTypesImportSectionNamespaceScopeProvider
import org.eclipse.xtext.scoping.impl.ImportNormalizer

@SuppressWarnings("restriction")
class UiModelJvmTypesImportSectionNamespaceScopeProvider extends OXJvmTypesImportSectionNamespaceScopeProvider {

	@Inject
	private IScopingInfoProvider infoProvider;

	override List<ImportNormalizer> getImplicitImports(boolean ignoreCase) {
		val List<ImportNormalizer> temp = super.getImplicitImports(ignoreCase)
		temp.add(new ImportNormalizer(qualifiedNameConverter.toQualifiedName("java.util"), true, ignoreCase));
		temp.add(
			new ImportNormalizer(qualifiedNameConverter.toQualifiedName("org.eclipse.osbp.ui.common"), true, ignoreCase));
		return temp;
	}

	override List<ImportNormalizer> internalGetImportedNamespaceResolvers(EObject context, boolean ignoreCase) {
		val List<ImportNormalizer> result = new ArrayList<ImportNormalizer>(
			super.internalGetImportedNamespaceResolvers(context, ignoreCase));

		if (context instanceof UiModel) {
			val String packageName = infoProvider.getPackageName(context);
			if (packageName != null && !packageName.equals("")) {
				result.add(new ImportNormalizer(qualifiedNameConverter.toQualifiedName(packageName), true, ignoreCase));
			}
		}

		return result;
	}

}
