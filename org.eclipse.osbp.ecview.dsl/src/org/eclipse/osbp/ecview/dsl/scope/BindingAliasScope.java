/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.scope;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBinding;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAlias;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTypedBindableDef;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxElementDefinition;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxEndpointDef;

public class BindingAliasScope extends AbstractScope {

	private UiBinding uiBinding;
	private IScope parent;
	private EReference reference;

	protected BindingAliasScope(IScope parent, UiBinding context,
			EReference reference) {
		super(IScope.NULLSCOPE, true);
		this.parent = parent;
		uiBinding = context;
		this.reference = reference;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		boolean filterType = reference == UiModelPackage.Literals.UI_BINDING__TARGET_ALIAS;
		if (filterType) {
			UiTypedBindableDef sourceTypedBindableDef = (UiTypedBindableDef) uiBinding
					.getSource();
			UxEndpointDef uxSourceEndpointDef = (UxEndpointDef) sourceTypedBindableDef
					.getMethod();
//			UiRawBindable bindable = sourceTypedBindableDef
//					.getRawBindablePath().getRawBindableOfLastSegment();
			if (uxSourceEndpointDef == null) {
				return parent.getAllElements();
			}

			List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
			for (IEObjectDescription des : parent.getAllElements()) {
				UiBindingEndpointAlias uiBindingEndpointAlias = (UiBindingEndpointAlias) des
						.getEObjectOrProxy();
				uiBindingEndpointAlias = (UiBindingEndpointAlias) EcoreUtil
						.resolve(uiBindingEndpointAlias, uiBinding);
				UiBindingEndpointAssignment endpointDef = (UiBindingEndpointAssignment) uiBindingEndpointAlias
						.getEndpoint();
				UiTypedBindableDef typedBindableDef = (UiTypedBindableDef) endpointDef
						.getTypedBindableDef();
				UxElementDefinition elementDef = (UxElementDefinition) typedBindableDef
						.getMethod().eContainer();

				if (uxSourceEndpointDef.eClass().isSuperTypeOf(
						elementDef.eClass())) {
					result.add(EObjectDescription.create(
							uiBindingEndpointAlias.getAlias(),
							uiBindingEndpointAlias));
				}
			}
			return result;
		} else {
			return parent.getAllElements();
		}
	}
}
