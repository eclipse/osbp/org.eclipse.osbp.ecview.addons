/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.scope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.ecview.semantic.uimodel.UiRawBindable;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTypedBindableDef;
import org.eclipse.osbp.ecview.semantic.uimodel.UiView;

public class BindingEndpointDefRawBindableScope extends AbstractScope {

	private EObject context;
	@SuppressWarnings("unused")
	private IQualifiedNameProvider nameProvider;

	protected BindingEndpointDefRawBindableScope(EObject context,
			IQualifiedNameProvider nameProvider) {
		super(IScope.NULLSCOPE, true);
		this.context = context;
		this.nameProvider = nameProvider;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		if (context instanceof UiTypedBindableDef) {
			UiTypedBindableDef def = (UiTypedBindableDef) context;
			List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
			result.add(EObjectDescription.create("this",
					findRawBindableParent(def)));
			UiView view = findView(def);
			if (view != null) {
				result.add(EObjectDescription.create(view.getName(), view));
			}

			// result.addAll(collectRawBindables(def));
			return result;
		}

		return Collections.emptyList();
	}

	private UiRawBindable findRawBindableParent(EObject eObject) {
		UiRawBindable result = null;
		while (eObject != null && eObject.eContainer() != null) {
			if (eObject instanceof UiRawBindable) {
				result = (UiRawBindable) eObject;
				break;
			}
			eObject = eObject.eContainer();
		}
		return result;
	}

	private UiView findView(EObject def) {
		UiView result = null;
		while (def.eContainer() != null) {
			def = def.eContainer();
			if (def instanceof UiView) {
				result = (UiView) def;
				break;
			}
		}
		return result;
	}
}
