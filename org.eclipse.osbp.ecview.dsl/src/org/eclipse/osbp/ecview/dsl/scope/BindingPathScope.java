/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.scope;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.ecview.dsl.extensions.OperationExtensions;
import org.eclipse.osbp.ecview.dsl.extensions.OperationExtensions.OperationInfo;

public class BindingPathScope extends AbstractScope {

	private JvmType type;

	protected BindingPathScope(JvmType type) {
		super(IScope.NULLSCOPE, true);
		this.type = (JvmType) type;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		if (type instanceof JvmDeclaredType) {

			Map<String, OperationInfo> infos = OperationExtensions
					.getOperationInfos((JvmDeclaredType) type);
			// apply readonly and create descriptions
			for (OperationInfo info : infos.values()) {
				if(info.getGetter() == null){
					continue;
				}
				result.add(EObjectDescription.create(info.getName(),
						info.getGetter()));
			}
		}
		return result;
	}
}
