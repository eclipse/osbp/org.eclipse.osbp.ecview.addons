/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.scope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.osbp.ecview.semantic.uimodel.UiRawBindable;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTypedBindableDef;
import org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsPackage;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxAvailableBindings;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxBindingableOption;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxElementDefinition;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class BindingEndpointDefBindMethodScope extends AbstractScope {

	private UiTypedBindableDef bindingEndpointDef;
	private IResourceDescriptions desc;

	protected BindingEndpointDefBindMethodScope(IResourceDescriptions desc,
			UiTypedBindableDef context) {
		super(IScope.NULLSCOPE, true);
		this.desc = desc;
		bindingEndpointDef = context;
	}

//	@Override
//	protected Iterable<IEObjectDescription> getAllLocalElements() {
//		UiRawBindable yEmb = bindingEndpointDef.getRawBindableOfLastSegment();
//		if (yEmb == null) {
//			return Collections.emptyList();
//		}
//		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
//		for (IEObjectDescription des : desc
//				.getExportedObjectsByType(UiSemanticsPackage.Literals.UX_BINDINGABLE_OPTION)) {
//			UxEndpointDef binding = (UxEndpointDef) des.getEObjectOrProxy();
//			binding = (UxEndpointDef) EcoreUtil.resolve(binding,
//					bindingEndpointDef);
//			EObject parent = binding.eContainer();
//			if (parent == null) {
//				continue;
//			}
//			UxElementDefinition elementDef = (UxElementDefinition) parent
//					.eContainer();
//			if (elementDef.getUri().getEClass().isSuperTypeOf(yEmb.eClass())) {
//				result.add(EObjectDescription.create(binding.getName(), binding));
//			}
//		}
//		return result;
//	}
	
	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		UiRawBindable yEmb = bindingEndpointDef.getRawBindableOfLastSegment();
		if (yEmb == null) {
			return Collections.emptyList();
		}
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		for (IEObjectDescription des : desc
				.getExportedObjectsByType(UiSemanticsPackage.Literals.UX_AVAILABLE_BINDINGS)) {
			UxAvailableBindings available = (UxAvailableBindings) des.getEObjectOrProxy();
			available = (UxAvailableBindings) EcoreUtil.resolve(available,
					bindingEndpointDef);
			UxElementDefinition elementDef = (UxElementDefinition) available.eContainer();
			if (elementDef == null) {
				continue;
			}
			List<String> targets = new ArrayList<>(available.getBindings().size() * 2);
			if (elementDef.getUri().getEClass().isSuperTypeOf(yEmb.eClass())) {
				for (UxBindingableOption binding : available.getBindings()) {
					if(targets.contains(binding.getTargetName() != null ? binding.getTargetName() : binding.getName())) {
						continue;
					}
					targets.add(binding.getTargetName() != null ? binding.getTargetName() : binding.getName());
					result.add(EObjectDescription.create(binding.getName(), binding));
				}
			}
			
		}
		return result;
	}
}
