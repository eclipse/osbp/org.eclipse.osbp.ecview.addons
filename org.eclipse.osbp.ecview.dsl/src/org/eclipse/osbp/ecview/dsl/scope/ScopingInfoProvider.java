/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.scope;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel;
import org.eclipse.osbp.xtext.oxtype.scoping.AbstractScopingInfoProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.scoping.impl.ImportNormalizer;
import org.eclipse.xtext.xbase.scoping.AbstractNestedTypeAwareImportNormalizer;

@SuppressWarnings("restriction")
public class ScopingInfoProvider extends AbstractScopingInfoProvider {

	@Override
	public String getPackageName(EObject context) {
		if (context instanceof UiModel) {
			return ((UiModel) context).getPackageName();
		}

		if (context.eContainer() == null) {
			return "";
		}

		return getPackageName(context.eContainer());
	}

	@Override
	public List<ImportNormalizer> getImplicitImports(boolean ignoreCase) {
		return Collections.emptyList();
	}

	@SuppressWarnings("restriction")
	protected ImportNormalizer doCreateImportNormalizer(
			QualifiedName importedNamespace, boolean wildcard,
			boolean ignoreCase) {
		return AbstractNestedTypeAwareImportNormalizer
				.createNestedTypeAwareImportNormalizer(importedNamespace,
						wildcard, ignoreCase);
	}
}
