/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.scope;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.common.types.TypesPackage;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.ImportNormalizer;
import org.eclipse.xtext.scoping.impl.ImportedNamespaceAwareLocalScopeProvider;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;

import com.google.inject.Inject;

public class UiImportedNamespaceAwareLocalScopeProvider extends
		ImportedNamespaceAwareLocalScopeProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	@Override
	protected List<ImportNormalizer> getImplicitImports(boolean ignoreCase) {
		List<ImportNormalizer> temp = new ArrayList<ImportNormalizer>(
				super.getImplicitImports(ignoreCase));
		temp.add(new ImportNormalizer(qualifiedNameConverter
				.toQualifiedName("java.util"), true, ignoreCase));
		temp.add(new ImportNormalizer(qualifiedNameConverter
				.toQualifiedName("java.lang"), true, ignoreCase));
		temp.add(new ImportNormalizer(qualifiedNameConverter
				.toQualifiedName("org.eclipse.osbp.ui.common"), true, ignoreCase));
		return temp;
	}

	@Override
	protected List<ImportNormalizer> internalGetImportedNamespaceResolvers(
			EObject context, boolean ignoreCase) {
		List<ImportNormalizer> result = new ArrayList<ImportNormalizer>(
				super.internalGetImportedNamespaceResolvers(context, ignoreCase));

		String packageName = getPackage(context);
		if (packageName != null && !packageName.equals("")) {
			result.add(new ImportNormalizer(qualifiedNameConverter
					.toQualifiedName(packageName), true, ignoreCase));
		}

		return result;
	}

	@Override
	public IScope getScope(EObject context, EReference reference) {
		EClass referenceType = reference.getEReferenceType();
		if (TypesPackage.Literals.JVM_TYPE.isSuperTypeOf(referenceType)) {
			UiModel model = null; 
			EObject temp = context;
			while(temp.eContainer() != null){
				temp = temp.eContainer();
				if(temp instanceof UiModel){
					model = (UiModel) temp;
					break;
				}
			}
			
			IScope result = getResourceScope(context.eResource(), reference);
			return getLocalElementsScope(result, model, reference);
		} else if (UiModelPackage.Literals.UI_RAW_BINDABLE.isSuperTypeOf(referenceType)) {
			IScope result = getResourceScope(context.eResource(), reference);
			return getLocalElementsScope(result, context, reference);
		}
		return super.getScope(context, reference);
	}

	/**
	 * Returns the package name.
	 * 
	 * @param eObject
	 * @return
	 */
	private String getPackage(EObject eObject) {
		if (eObject == null) {
			return "";
		}
		if (eObject instanceof UiModel) {
			return ((UiModel) eObject).getPackageName();
		} else {
			return getPackage(eObject.eContainer());
		}
	}
}
