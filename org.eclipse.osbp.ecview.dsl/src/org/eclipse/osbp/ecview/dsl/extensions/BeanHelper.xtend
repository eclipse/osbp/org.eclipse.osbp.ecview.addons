/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.extensions

import org.apache.commons.lang3.reflect.FieldUtils
import org.eclipse.osbp.runtime.common.annotations.DomainDescription
import org.eclipse.osbp.runtime.common.annotations.DomainKey

class BeanHelper {

	/**
	 * @param bean the bean
	 * @return the caption property if it could be found. Null otherwise.
	 */
	def static String findCaptionProperty(Object bean) {
		if (bean == null) {
			return null
		}

		if (bean instanceof Class<?>) {
			return findCaptionProperty(bean as Class<?>)
		} else {
			return findCaptionProperty(bean.class)
		}
	}

	/**
	 * @param beanClass the bean class
	 * @return the caption property if it could be found. Null otherwise.
	 */
	def static String findCaptionProperty(Class<?> beanClass) {
		if (beanClass == null) {
			return null
		}

		// include super classes too
		for (field : FieldUtils.getAllFields(beanClass)) {
			if (field.isAnnotationPresent(typeof(DomainKey))) {
				return field.name
			}
		}

		return null
	}

	/**
	 * @param bean the bean
	 * @return the description property if it could be found. Null otherwise.
	 */
	def static String findDescriptionProperty(Object bean) {
		if (bean == null) {
			return null
		}

		if (bean instanceof Class<?>) {
			return findDescriptionProperty(bean as Class<?>)
		} else {
			return findDescriptionProperty(bean.class)
		}
	}

	/**
	 * @param beanClass the generic bean class
	 * @return the description property if it could be found. Null otherwise.
	 */
	def static String findDescriptionProperty(Class<?> beanClass) {
		if (beanClass == null) {
			return null
		}

		// try to find annotation in fields of class
		for (field : beanClass.declaredFields) {
			if (field.isAnnotationPresent(typeof(DomainDescription))) {
				return field.name
			}
		}

		// try to find annotation in methods of class
		for (method : beanClass.declaredMethods) {
			if (method.isAnnotationPresent(typeof(DomainDescription))) {
				return OperationExtensions.toPropertyName(method.name)
			}
		}

		// include super classes too
		for (field : FieldUtils.getAllFields(beanClass)) {
			if (field.isAnnotationPresent(typeof(DomainDescription))) {
				return field.name
			}
		}

		// include super class too
		for (method : beanClass.methods) {
			if (method.isAnnotationPresent(typeof(DomainDescription))) {
				return OperationExtensions.toPropertyName(method.name)
			}
		}

		return null
	}

}
