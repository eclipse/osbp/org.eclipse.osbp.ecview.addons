/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.extensions

import com.google.inject.Inject
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanReferenceField
import org.eclipse.osbp.ecview.semantic.uimodel.UiColumn
import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiEmbeddable
import org.eclipse.osbp.ecview.semantic.uimodel.UiErrorCode
import org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction
import org.eclipse.osbp.ecview.semantic.uimodel.UiField
import org.eclipse.osbp.ecview.semantic.uimodel.UiList
import org.eclipse.osbp.ecview.semantic.uimodel.UiNestedProperty
import org.eclipse.osbp.ecview.semantic.uimodel.UiOptionsGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchField
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiTable
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmType
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.osbp.ecview.core.common.model.core.util.II18nConstants

class I18nKeyProvider {

	@Inject extension IQualifiedNameProvider
	@Inject BindingInfoHelper bindingInfoHelper;

	def dispatch String toI18nKey(UiEmbeddable embeddable) {
		if(embeddable.i18nInfo !== null && embeddable.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (embeddable.i18nInfo !== null && embeddable.i18nInfo.key !== null) {
			return embeddable.i18nInfo.key;
		}

		if (embeddable instanceof UiField) {
			val String i18nKey = toI18nKeyByBindings(embeddable)
			if (i18nKey !== null && !i18nKey.equals("")) {
				return i18nKey;
			}
		}
		return if(embeddable.name !== null) embeddable.name else ""
	}

	def dispatch String toI18nKey(UiTable element) {
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiList element) {
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiOptionsGroup element) {
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiComboBox element) {
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiSearchPanel element) {
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiBeanReferenceField element) {
		
		if(element.i18nInfo !== null && element.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (element.i18nInfo !== null && element.i18nInfo.key !== null) {
			return element.i18nInfo.key;
		} else if (element.jvmType !== null) {
			return element.jvmType.simpleName
		}
		return if(element.name !== null) element.name else ""
	}

	def dispatch String toI18nKey(UiColumn column) {
		val UiTable table = column.eContainer.eContainer as UiTable

		if (column.i18nInfo !== null && column.i18nInfo.key !== null) {
			return column.i18nInfo.key;
		} else if (table.jvmType !== null) {
			val UiNestedProperty property = column.property
			if (property !== null) {
				val path = property.toPathString
				if (path === null) {
					return table.jvmType.simpleName
				} else if (path.contains(".")) {
					return property.simpleGetterNameOfLastSegment
				} else {
					return property.simpleGetterName
				}
			}
		} else {
			val UiNestedProperty property = column.property
			if (property !== null) {
				val parent = column.eContainer().eContainer() as UiEmbeddable
				return parent.name + "." + property.toPathString
			}
		}
		return column.name
	}

	def dispatch String toI18nKey(UiExposedAction action) {
		if (action.actionReference !== null) {
			return action.actionReference.name
		} else {
			return action.actionID
		}
	}

	def dispatch String toI18nKey(UiSearchField embeddable) {
		if(embeddable.i18nInfo !== null && embeddable.i18nInfo.noCaption){
			return II18nConstants.NO_CAPTION
		} 
		
		if (embeddable.i18nInfo !== null && embeddable.i18nInfo.key !== null) {
			return embeddable.i18nInfo.key;
		} else {
			val UiNestedProperty property = embeddable.property
			if (property !== null) {
				val String i18nKey = toI18nKeyByBindings(property)
				if (i18nKey !== null && !i18nKey.equals("")) {
					return i18nKey;
				}
				return "search." + property.toPathString
			}
		}
		return embeddable.name
	}

	def dispatch String toI18nKey(UiTabAssignment embeddable) {
		if (embeddable.i18nInfo !== null && embeddable.i18nInfo.key !== null) {
			return embeddable.i18nInfo.key;
		}
		return embeddable.name
	}

	def dispatch String toI18nKey(UiErrorCode code) {
		val fqn = code.fullyQualifiedName
		return fqn.skipLast(1).toString
	}

	def String toI18nKeyByBindings(UiEmbeddable embeddable) {
		if (embeddable instanceof UiField) {
			val ops = bindingInfoHelper.findBoundOppositeJvmOperations(embeddable, "value")

			if (!ops.empty) {
				val JvmOperation op = ops.get(0)
				if(op === null){
					return null
				}
				val JvmType type = op.declaringType
				if (type === null) {
					return null
				}
				return ops.get(0).simpleName.toPropertyName
			}
		}
	}

	def String toI18nKeyByBindings(UiNestedProperty property) {
		if (property !== null) {
			val JvmOperation op = property.operationofLastSegment
			if (op !== null) {
				val JvmType type = op.declaringType
				if (type === null) {
					return null
				}
				return op.simpleName.toPropertyName
			}
		}
	}

	def static getToPropertyName(String string) {
		if (string.startsWith("get")) {
			return string.replaceFirst("get", "").toFirstLower
		} else if (string.startsWith("is")) {
			return string.replaceFirst("is", "").toFirstLower
		}
	}
}
