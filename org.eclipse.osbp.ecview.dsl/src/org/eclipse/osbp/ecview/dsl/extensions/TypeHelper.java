/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.extensions;

import java.util.Collection;
import java.util.Date;

import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmEnumerationType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.osbp.ecview.dsl.extensions.OperationExtensions.OperationInfo;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;

import com.google.inject.Inject;
import com.google.inject.Singleton;

// TODO: Auto-generated Javadoc
/**
 * The Class TypeHelper.
 */
@SuppressWarnings({ "restriction", "deprecation" })
@Singleton
public class TypeHelper {

	/** The type references. */
	@Inject
	private TypeReferences typeReferences;

	/** The super type collector. */
	@Inject
	private SuperTypeCollector superTypeCollector;

	/**
	 * Returns true, if the type is numeric without digits.
	 *
	 * @param type
	 *            the type
	 * @return true, if is number
	 */
	public boolean isNumber(JvmType type) {
		if (typeReferences.is(type, Byte.class) // NOSONAR
				|| typeReferences.is(type, Byte.TYPE)
				|| typeReferences.is(type, Short.class)
				|| typeReferences.is(type, Short.TYPE)
				|| typeReferences.is(type, Integer.class)
				|| typeReferences.is(type, Integer.TYPE)
				|| typeReferences.is(type, Long.class)
				|| typeReferences.is(type, Long.TYPE)
				|| typeReferences.is(type, Float.class)
				|| typeReferences.is(type, Float.TYPE)
				|| typeReferences.is(type, Double.class)
				|| typeReferences.is(type, Double.TYPE)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true, if the type is numeric.
	 *
	 * @param type
	 *            the type
	 * @return true, if is number without digits
	 */
	public boolean isNumberWithoutDigits(JvmType type) {
		if (typeReferences.is(type, Byte.class)            // NOSONAR
				|| typeReferences.is(type, Byte.TYPE)
				|| typeReferences.is(type, Short.class)
				|| typeReferences.is(type, Short.TYPE)
				|| typeReferences.is(type, Integer.class)
				|| typeReferences.is(type, Integer.TYPE)
				|| typeReferences.is(type, Long.class)
				|| typeReferences.is(type, Long.TYPE)) { 
			return true;
		}
		return false;
	}

	/**
	 * Returns true, if the type is numeric with digits.
	 *
	 * @param type
	 *            the type
	 * @return true, if is number with digits
	 */
	public boolean isNumberWithDigits(JvmType type) {
		if (typeReferences.is(type, Float.class)
				|| typeReferences.is(type, Float.TYPE)
				|| typeReferences.is(type, Double.class)
				|| typeReferences.is(type, Double.TYPE)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the qualified name of the given numeric type. Throws exception,
	 * if the given type is not a valid number.
	 *
	 * @param type
	 *            the type
	 * @return the string
	 */
	public String toNumericQualifiedName(JvmType type) {
		return toNumericType(type).getName();
	}

	/**
	 * Returns the class file of the given numeric type. Throws exception, if
	 * the given type is not a valid number.
	 *
	 * @param type
	 *            the type
	 * @return the class&lt;? extends number&gt;
	 */
	public Class<? extends Number> toNumericType(JvmType type) { //NOSONAR
		if (typeReferences.is(type, Byte.class)
				|| typeReferences.is(type, Byte.TYPE)) {
			return Byte.class;
		} else if (typeReferences.is(type, Short.class)
				|| typeReferences.is(type, Short.TYPE)) {
			return Short.class;
		} else if (typeReferences.is(type, Integer.class)
				|| typeReferences.is(type, Integer.TYPE)) {
			return Integer.class;
		} else if (typeReferences.is(type, Long.class)
				|| typeReferences.is(type, Long.TYPE)) {
			return Long.class;
		} else if (typeReferences.is(type, Float.class)
				|| typeReferences.is(type, Float.TYPE)) {
			return Float.class;
		} else if (typeReferences.is(type, Double.class)
				|| typeReferences.is(type, Double.TYPE)) {
			return Double.class;
		}
		throw new IllegalArgumentException(type + " is not a valid number type");
	}

	/**
	 * Returns true, if the type is boolean.
	 *
	 * @param type
	 *            the type
	 * @return true, if is boolean
	 */
	public boolean isBoolean(JvmType type) {
		if (typeReferences.is(type, Boolean.class)
				|| typeReferences.is(type, Boolean.TYPE)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true, if the type is boolean.
	 *
	 * @param type
	 *            the type
	 * @return true, if is string
	 */
	public boolean isString(JvmType type) {
		if (typeReferences.is(type, String.class)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true, if the type is date.
	 *
	 * @param type
	 *            the type
	 * @return true, if is date
	 */
	public boolean isDate(JvmType type) {
		if (typeReferences.is(type, Date.class)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true, if the type is enum.
	 *
	 * @param type
	 *            the type
	 * @return true, if is enum
	 */
	public boolean isEnum(JvmType type) {
		return type instanceof JvmEnumerationType;
	}

	/**
	 * Returns true, if the type is enum.
	 *
	 * @param info
	 *            the info
	 * @return true, if is domain reference
	 */
	public boolean isDomainReference(OperationInfo info) {
		JvmField field = info.getField();
		if (field == null) {
			return false;
		}

		if (isCollection(field)) {
			return false;
		}

		for (JvmAnnotationReference ref : field.getAnnotations()) {
			if (typeReferences.is(ref.getAnnotation(), DomainReference.class)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks if is collection.
	 *
	 * @param field
	 *            the field
	 * @return true, if is collection
	 */
	public boolean isCollection(JvmField field) {
		JvmType fieldType = field.getType().getType();
		if (!(fieldType instanceof JvmDeclaredType)) {
			// in case of proxy
			return false;
		}
		JvmType collectionType = typeReferences.findDeclaredType(
				Collection.class, field);
		if (!(collectionType instanceof JvmDeclaredType)) {
			// in case of proxy
			return false;
		}

		if (superTypeCollector.isSuperType((JvmDeclaredType) fieldType,
				(JvmDeclaredType) collectionType)) {
			return true;
		}
		return false;
	}
}
