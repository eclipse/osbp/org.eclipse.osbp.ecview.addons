/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.dsl.derivedstate;

import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.osbp.ecview.semantic.uimodel.UiEmbeddable;
import org.eclipse.osbp.ecview.semantic.uimodel.util.UiModelUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class UiModelGrammarUtil.
 */
@SuppressWarnings("restriction")
public class UiModelGrammarUtil {

	/**
	 * Returns the grammar model element for the given view model element.
	 *
	 * @param yElement
	 *            the y element
	 * @return the ui grammar element
	 */
	public static EObject getUiGrammarElement(EObject yElement) {
		if (yElement == null)
			return null;
		List<Adapter> adapters = yElement.eAdapters();
		for (int i = 0; i < adapters.size(); i++) {
			Adapter adapter = adapters.get(i);
			if (adapter instanceof IUiGrammarElement)
				return ((IUiGrammarElement) adapter).getGrammarElement();
		}
		return null;
	}

	/**
	 * Returns the grammar model element for the given view model element.
	 *
	 * @param yElement
	 *            the y element
	 * @return the xtext node
	 */
	public static CompositeNodeWithSemanticElement getXtextNode(EObject yElement) {
		if (yElement == null)
			return null;

		EObject uiGrammarElement = getUiGrammarElement(yElement);
		if (uiGrammarElement == null)
			return null;

		return (CompositeNodeWithSemanticElement) NodeModelUtils
				.getNode(uiGrammarElement);
	}

	/**
	 * Returns the path for the current element up to the view will be concated
	 * by "." Elements with no name are skipped. For some elements their unique
	 * id may be returned.
	 *
	 * @param embeddable
	 *            the embeddable
	 * @return the path id
	 */
	public static String getPathId(UiEmbeddable embeddable) {
		return UiModelUtil.getPathId(embeddable);
	}
}
