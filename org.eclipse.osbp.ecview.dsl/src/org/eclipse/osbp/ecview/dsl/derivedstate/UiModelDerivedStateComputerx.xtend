/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.derivedstate

import com.google.inject.Inject
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.Stack
import org.apache.commons.lang.StringEscapeUtils
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory
import org.eclipse.osbp.ecview.core.common.model.core.YAlignment
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotListBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint
import org.eclipse.osbp.ecview.core.common.model.core.YDeviceType
import org.eclipse.osbp.ecview.core.common.model.core.YDialog
import org.eclipse.osbp.ecview.core.common.model.core.YElement
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable
import org.eclipse.osbp.ecview.core.common.model.core.YExposedAction
import org.eclipse.osbp.ecview.core.common.model.core.YField
import org.eclipse.osbp.ecview.core.common.model.core.YFlatAlignment
import org.eclipse.osbp.ecview.core.common.model.core.YLayout
import org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand
import org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand
import org.eclipse.osbp.ecview.core.common.model.core.YView
import org.eclipse.osbp.ecview.core.common.model.core.YViewSet
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidator
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidator
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator
import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidator
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor
import org.eclipse.osbp.ecview.core.^extension.model.datatypes.YDateTimeFormat
import org.eclipse.osbp.ecview.core.^extension.model.datatypes.YDateTimeResolution
import org.eclipse.osbp.ecview.core.^extension.model.^extension.ExtensionModelFactory
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YAddToTableCommand
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YBeanReferenceField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YBrowser
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YButton
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YCheckBox
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YColumn
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YComboBox
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YDateTime
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YDecimalField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YEnumComboBox
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YEnumList
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YEnumOptionsGroup
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YFormLayout
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YGridLayout
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YHorizontalLayout
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YImage
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YLabel
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YList
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YNumericField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YOptionsGroup
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YPanel
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YPasswordField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YProgressBar
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YRemoveFromTableCommand
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSearchPanel
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSelectionType
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSetNewBeanInstanceCommand
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSortColumn
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSplitPanel
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSuggestTextField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YTab
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YTabSheet
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YTable
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YTextArea
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YTextField
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YVerticalLayout
import org.eclipse.osbp.ecview.core.^extension.model.^extension.util.SimpleExtensionModelFactory
import org.eclipse.osbp.ecview.dsl.autowire.hook.ExtensionsAutowireDelegate
import org.eclipse.osbp.ecview.dsl.extensions.BeanHelper
import org.eclipse.osbp.ecview.dsl.extensions.BindingInfoHelper
import org.eclipse.osbp.ecview.dsl.extensions.I18nKeyProvider
import org.eclipse.osbp.ecview.dsl.extensions.OperationExtensions
import org.eclipse.osbp.ecview.dsl.extensions.TypeHelper
import org.eclipse.osbp.ecview.^extension.model.YECviewFactory
import org.eclipse.osbp.ecview.^extension.model.YRichTextArea
import org.eclipse.osbp.ecview.semantic.uimodel.UiAddToTableCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiAlignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanReferenceField
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanSlot
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanValidationValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiBinding
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAlias
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiBrowser
import org.eclipse.osbp.ecview.semantic.uimodel.UiButton
import org.eclipse.osbp.ecview.semantic.uimodel.UiCheckBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiColumn
import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiDateField
import org.eclipse.osbp.ecview.semantic.uimodel.UiDateFormat
import org.eclipse.osbp.ecview.semantic.uimodel.UiDateTimeResolution
import org.eclipse.osbp.ecview.semantic.uimodel.UiDecimalField
import org.eclipse.osbp.ecview.semantic.uimodel.UiDialog
import org.eclipse.osbp.ecview.semantic.uimodel.UiDialogAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiDialogSearchFieldAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiEmbeddable
import org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction
import org.eclipse.osbp.ecview.semantic.uimodel.UiField
import org.eclipse.osbp.ecview.semantic.uimodel.UiFlatAlignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayoutAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayoutAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroupAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayoutAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiIDEView
import org.eclipse.osbp.ecview.semantic.uimodel.UiImage
import org.eclipse.osbp.ecview.semantic.uimodel.UiLabel
import org.eclipse.osbp.ecview.semantic.uimodel.UiLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiList
import org.eclipse.osbp.ecview.semantic.uimodel.UiMaxLengthValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiMinLengthValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavBarAction
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationButton
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationHandler
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPage
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPageAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRoot
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRootAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileSearchPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileView
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel
import org.eclipse.osbp.ecview.semantic.uimodel.UiNamedElement
import org.eclipse.osbp.ecview.semantic.uimodel.UiNestedProperty
import org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField
import org.eclipse.osbp.ecview.semantic.uimodel.UiOpenDialogCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiOptionsGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiPasswordField
import org.eclipse.osbp.ecview.semantic.uimodel.UiPathSegment
import org.eclipse.osbp.ecview.semantic.uimodel.UiPoint
import org.eclipse.osbp.ecview.semantic.uimodel.UiProgressBar
import org.eclipse.osbp.ecview.semantic.uimodel.UiRegexpValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiRemoveFromTableCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiRichTextArea
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchDialog
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchField
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchWithDialogCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiSelectionType
import org.eclipse.osbp.ecview.semantic.uimodel.UiSendEventCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiSetNewInstanceCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiSortOrder
import org.eclipse.osbp.ecview.semantic.uimodel.UiSplitpanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiSplitpanelAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiSuggestTextField
import org.eclipse.osbp.ecview.semantic.uimodel.UiSwitch
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiTable
import org.eclipse.osbp.ecview.semantic.uimodel.UiTextArea
import org.eclipse.osbp.ecview.semantic.uimodel.UiTextField
import org.eclipse.osbp.ecview.semantic.uimodel.UiValidatorAlias
import org.eclipse.osbp.ecview.semantic.uimodel.UiValidatorAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiValidatorDef
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroupAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayoutAssigment
import org.eclipse.osbp.ecview.semantic.uimodel.UiView
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessor
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiXbaseValidator
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMHorizontalButtonGroup
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMNavigationBarButton
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMNavigationButton
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMNavigationCommand
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMNavigationPage
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMNavigationRoot
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMSearchPanel
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMSwitch
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMTab
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMTabSheet
import org.eclipse.osbp.mobile.vaadin.ecview.model.VMVerticalComponentGroup
import org.eclipse.osbp.mobile.vaadin.ecview.model.VaadinMobileFactory
import org.eclipse.osbp.runtime.common.annotations.Properties
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoader
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory
import org.eclipse.xtext.common.types.JvmAnnotationAnnotationValue
import org.eclipse.xtext.common.types.JvmAnnotationReference
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmEnumerationType
import org.eclipse.xtext.common.types.JvmField
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmStringAnnotationValue
import org.eclipse.xtext.common.types.JvmType
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.resource.DerivedStateAwareResource
import org.eclipse.xtext.xbase.jvmmodel.JvmModelAssociator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@SuppressWarnings("restriction")
class UiModelDerivedStateComputerx extends JvmModelAssociator {

	private static final Logger LOGGER = LoggerFactory.getLogger(typeof(UiModelDerivedStateComputerx));

	@Inject
	ITypeLoaderFactory typeLoaderFactory;
	ITypeLoader typeLoader

	@Inject BindingInfoHelper bindingInfoHelper;
	@Inject TypeHelper typeHelper;
	@Inject extension IQualifiedNameProvider;
	@Inject I18nKeyProvider i18nKeyProvider
	@Inject ExtensionsAutowireDelegate autowireHelper
	

	final Stack<EObject> viewContext = new Stack
	final List<YView> views = newArrayList()
	final Map<EObject, EObject> grammarToUiAssociations = newHashMap();
	final Map<EObject, EObject> uiToGrammarAssociations = newHashMap();
	final SimpleExtensionModelFactory factory = new SimpleExtensionModelFactory

	String currentPackage
	YView currentView;

	DerivedStateAwareResource resource
	List<Runnable> pendingMappings = newArrayList()
	List<UiBinding> pendingBindings = newArrayList()
	List<UiBinding> temporaryPendingBindings = newArrayList()
	List<UiVisibilityProcessorAssignment> pendingVisibilityProcessors = newArrayList()

	List<UiLayout> pendingAutowires = newArrayList()

	def void associateUi(EObject grammarElement, EObject uiElement) {
		grammarToUiAssociations.put(grammarElement, uiElement)
		uiToGrammarAssociations.put(uiElement, grammarElement)

		uiElement.eAdapters += new UiGrammarElementAdapter(grammarElement)
	}

	def <A> A associatedUi(EObject grammarElement) {
		return grammarToUiAssociations.get(grammarElement) as A
	}

	def <A> A associatedGrammar(EObject uiElement) {
		return uiToGrammarAssociations.get(uiElement) as A
	}

	def YView getCurrentView() {
		currentView
	}

	override void installDerivedState(DerivedStateAwareResource resource, boolean preLinkingPhase) {

		super.installDerivedState(resource, preLinkingPhase)

		if (resource.getContents().isEmpty()) {
			return;
		}

		if (!preLinkingPhase) {
			val UiModel uiModel = resource.getContents().get(0) as UiModel;
			uiModel.installDerivedStateFor(resource, preLinkingPhase)
		}
	}

	def void installDerivedStateFor(UiModel eObject, DerivedStateAwareResource resource, boolean preLinkingPhase) {

		super.installDerivedState(resource, preLinkingPhase)
		this.resource = resource;
		this.typeLoader = typeLoaderFactory.createTypeLoader(resource.resourceSet)

		if (resource.getContents().isEmpty()) {
			return;
		}

		if (!preLinkingPhase) {

			grammarToUiAssociations.clear
			uiToGrammarAssociations.clear
			pendingAutowires.clear

			currentPackage = eObject.packageName

			try {

				// complete all elements
				eObject.eContents.forEach [
					it.map
				]
			} catch (Exception ex) {
				LOGGER.error("{}", ex)
			}

			if (views.size > 0) {
				resource.contents.addAll(views)
			}
			views.clear
			viewContext.clear
			pendingBindings.clear
			pendingMappings.clear
			pendingVisibilityProcessors.clear
			pendingAutowires.clear
		}

		this.typeLoader.dispose
		this.typeLoader = null;
	}

	def <A> A peek() {
		viewContext.peek as A
	}

	def <A> A pop() {
		viewContext.pop as A
	}

	def String toI18nKey(UiNamedElement element) {
		return i18nKeyProvider.toI18nKey(element)
	}

	def String toI18nKey(UiEmbeddable element) {
		return i18nKeyProvider.toI18nKey(element)
	}

	def dispatch void map(UiModel object) {
		currentPackage = object.packageName
		object.roots.filter[!(it instanceof UiValidatorAlias)].forEach[it.map]
	}

	def dispatch void map(UiView object) {
	}

	def dispatch void map(Void object) {
		println("void")
	}

	def dispatch void map(UiIDEView object) {

		// create a view instance
		val YView yView = factory.createView
		yView.id = object.fullyQualifiedName.toString
		yView.name = object.fullyQualifiedName.toString
		yView.viewName = object.fullyQualifiedName.toString
		yView.sharedStateGroup = object.sharedStateGroup
		yView.category = if(object.viewCategory !== null) object.viewCategory.name else null
		yView.contentAlignment = object.contentAlignment.toYAlignment
		object.associateUi(yView)
		views += yView
		currentView = yView
		yView.push;

		object.beanSlots.forEach[it.map]

		object.bindings.forEach[it.map]

		val element = object.content.create
		yView.content = element
		object.content.map

		object.exposedActions.forEach[it.map]

		pendingAutowires.forEach [
			it.doAutowire
		]

		object.processorAssignments.forEach [
			it.map
		]

		// process the visibility processors
		pendingVisibilityProcessors.forEach [
			it.processor.map
		]

		pendingMappings.forEach [
			it.run
		]

		// install all bindings
		processBindings

		pendingMappings.forEach [
			it.run
		]

		object.validatorAssignments.forEach[it.map]

		// install the I18nKeys based on bindings
		pop
		currentView = null
		pendingAutowires.clear
		pendingBindings.clear
		pendingMappings.clear
		pendingVisibilityProcessors.clear

	}

	def doAutowire(UiLayout embeddable) {
		autowireHelper.autowire(embeddable, this, currentView.deviceType === YDeviceType.MOBILE)
	}

	def dispatch void map(UiMobileView object) {

		// create a view instance
		val YView yView = factory.createView
		yView.name = object.fullyQualifiedName.toString
		yView.viewName = object.fullyQualifiedName.toString
		yView.sharedStateGroup = object.sharedStateGroup
		yView.deviceType = YDeviceType.MOBILE
		yView.contentAlignment = object.contentAlignment.toYAlignment
		object.associateUi(yView)
		views += yView
		currentView = yView
		yView.push;

		object.beanSlots.forEach[it.map]

		val element = object.content.create
		yView.content = element
		object.content.map

		object.bindings.forEach[it.map]

		pendingAutowires.forEach [
			it.doAutowire
		]

		object.processorAssignments.forEach [
			it.map
		]

		// process the visibility processors
		pendingVisibilityProcessors.forEach [
			it.processor.map
		]

		pendingMappings.forEach [
			it.run
		]

		// install all bindings
		processBindings

		// and do autowire again -> Bindings may installed autowires
		pendingAutowires.forEach [
			it.doAutowire
		]

		pendingMappings.forEach [
			it.run
		]

		object.validatorAssignments.forEach[it.map]

		pop
		currentView = null
		pendingAutowires.clear
		pendingBindings.clear
		pendingMappings.clear
		pendingVisibilityProcessors.clear

	}

	def void processBindings() {
		if (pendingBindings.empty) {
			return
		}

		// process bindings
		temporaryPendingBindings = newArrayList(pendingBindings)
		pendingBindings.clear
		temporaryPendingBindings.forEach [
			it.install
		]

		// again process bindings in case that process above created new one
		processBindings
	}

	def push(EObject eObject) {
		viewContext.push(eObject)
	}

	def dispatch void map(UiGridLayout eObject) {
		val YGridLayout yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiGridLayoutAssigment eObject) {
		val YGridLayout layout = peek

		val element = eObject.element
		val newField = element.create
		layout.addElement(newField)

		if (eObject.alignment !== UiAlignment.UNDEFINED) {
			val style = layout.addGridLayoutCellStyle(newField)
			style.alignment = eObject.alignment.toYAlignment
		}

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}
	}

	def dispatch void map(UiVerticalLayout eObject) {
		val YVerticalLayout yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiVerticalLayoutAssigment eObject) {

		val YVerticalLayout layout = peek

		val element = eObject.element
		val newField = element.create

		layout.addElement(newField)

		if (eObject.alignment !== UiAlignment.UNDEFINED) {
			val style = layout.addCellStyle(newField)
			style.alignment = eObject.alignment.toYAlignment
		}
		element.map

		if (element instanceof UiField) {
			if (newField !== null) {
				newField.push
				val UiField yField = element as UiField
				yField.validators.forEach [
					it.map
				]
				pop
			}
		}
	}

	def dispatch void map(UiMobileNavigationRoot eObject) {
		val VMNavigationRoot yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiMobileNavigationRootAssigment eObject) {

		val VMNavigationRoot layout = peek

		val element = eObject.element
		val newField = element.create

		layout.addElement(newField)

		element.map

		if (element instanceof UiField) {
			if (newField !== null) {
				newField.push
				val UiField yField = element as UiField
				yField.validators.forEach [
					it.map
				]
				pop
			}
		}
	}

	def dispatch void map(UiHorizontalLayout eObject) {
		val YHorizontalLayout yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		if (eObject.autowire) {
			pendingAutowires += eObject
		}

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiHorizontalLayoutAssigment eObject) {

		val YHorizontalLayout layout = peek
		val element = eObject.element
		val newField = element.create
		layout.addElement(newField)
		if (eObject.alignment !== UiAlignment.UNDEFINED) {
			val style = layout.addCellStyle(newField)
			style.alignment = eObject.alignment.toYAlignment
		}

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}
	}

	def dispatch void map(UiSearchPanel eObject) {
		val YSearchPanel yPanel = eObject.associatedUi
		yPanel.push

		eObject.contents.forEach [
			val newField = it.create
			if (newField === null) {
				return
			}
			yPanel.addElement(newField)
			it.map
			newField.push
			val UiField yField = it as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiMobileSearchPanel eObject) {
		val VMSearchPanel yPanel = eObject.associatedUi
		yPanel.push

		eObject.contents.forEach [
			val newField = it.create
			if (newField === null) {
				return
			}
			yPanel.addElement(newField)
			it.map
			newField.push
			val UiField yField = it as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiSplitpanel eObject) {
		val YSplitPanel yField = eObject.associatedUi
		yField.push

		eObject.firstContent.map
		eObject.secondContent.map

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiSplitpanelAssigment eObject) {

		val YSplitPanel layout = peek
		val element = eObject.element
		val newField = element.create

		if (newField === null) {
			return
		}

		layout.addElement(newField)

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}
	}

	def dispatch void map(UiPanel eObject) {
		val YPanel yPanel = eObject.associatedUi
		yPanel.push

		val element = eObject.content
		val newField = element.create

		if (newField === null) {
			return
		}

		yPanel.elements += newField
		element.map

		if (element instanceof UiField) {
			if (newField !== null) {
				newField.push
				val UiField yField = element as UiField
				yField.validators.forEach [
					it.map
				]
				pop
			}
		}

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiTabSheet eObject) {
		val YTabSheet yField = eObject.associatedUi
		yField.push

		eObject.tabs.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiTabAssignment eObject) {

		val YTabSheet layout = peek
		val YTab tab = factory.createTab
		tab.label = eObject.name
		tab.labelI18nKey = eObject.toI18nKey
		layout.tabs += tab

		tab.push

		val element = eObject.element
		val newField = element.create
		tab.embeddable = newField

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}

		pop

	}

	def dispatch void map(UiMobileTabSheet eObject) {
		val VMTabSheet yField = eObject.associatedUi
		yField.push

		eObject.tabs.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiMobileTabAssignment eObject) {

		val VMTabSheet layout = peek
		val VMTab tab = VaadinMobileFactory.eINSTANCE.createVMTab
		tab.label = eObject.name
		layout.tabs += tab

		tab.push

		val element = eObject.element
		val newField = element.create
		tab.embeddable = newField
		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop

	}

	def dispatch void map(UiHorizontalButtonGroup eObject) {
		val VMHorizontalButtonGroup yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiHorizontalButtonGroupAssigment eObject) {

		val VMHorizontalButtonGroup layout = peek
		val element = eObject.element
		val newField = element.create
		layout.addElement(newField)
		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}

	}

	def dispatch void map(UiVerticalComponentGroup eObject) {
		val VMVerticalComponentGroup yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiVerticalComponentGroupAssigment eObject) {

		val VMVerticalComponentGroup layout = peek
		val element = eObject.element
		val newField = element.create
		layout.addElement(newField)

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}
	}

	def dispatch void map(UiMobileNavigationPage eObject) {
		val VMNavigationPage yField = eObject.associatedUi

		yField.push

		eObject.barActions.forEach [
			val newField = it.create
			yField.barActions += newField as VMNavigationBarButton
			it.map
		]

		eObject.contents.forEach [
			it.map
		]

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiDialog eObject) {
		val YDialog layout = CoreModelFactory.eINSTANCE.createYDialog
		layout.name = eObject.name
		layout.label = eObject.name
		layout.labelI18nKey = eObject.toI18nKey

		if (eObject.jvmType !== null) {
			layout.typeQualifiedName = eObject.jvmType.qualifiedName
			layout.type = loadClass(eObject.eResource.resourceSet, eObject.jvmType.qualifiedName)
		}

		currentView.dialogs += layout

		eObject.associateUi(layout)

		layout.push

		eObject.content?.map

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiSearchDialog eObject) {
		val YDialog dialog = CoreModelFactory.eINSTANCE.createYDialog
		dialog.name = eObject.name
		dialog.label = eObject.name
		dialog.labelI18nKey = eObject.toI18nKey

		val YOpenDialogCommand command = peek
		if (command !== null) {
			command.dialog = dialog
		}

		if (eObject.jvmType !== null) {
			dialog.typeQualifiedName = eObject.jvmType.qualifiedName
			dialog.type = loadClass(eObject.eResource.resourceSet, eObject.jvmType.qualifiedName)
		}

		eObject.associateUi(dialog)
		currentView.dialogs += dialog
		dialog.push

		val YVerticalLayout content = ExtensionModelFactory.eINSTANCE.createYVerticalLayout
		dialog.content = content
		content.push

		val YGridLayout searchFieldLayout = ExtensionModelFactory.eINSTANCE.createYGridLayout
		content.elements += searchFieldLayout
		searchFieldLayout.push

		eObject.searchFields.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		// pop searchFieldLayout
		pop

		// map the content of the search dialog
		eObject.content?.map

		// pop content
		pop

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		// pop dialog
		pop
	}

	def dispatch void map(UiDialogAssignment eObject) {

		val YDialog yDialog = peek

		val element = eObject.element
		val newField = element.create
		yDialog.content = newField
		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}
	}

	def dispatch void map(UiSearchField eObject) {
		// nothing to do
	}

	def dispatch void map(UiDialogSearchFieldAssignment eObject) {

		val YLayout layout = peek
		val element = eObject.element

		val UiNestedProperty property = element.property
		if (property !== null) {
			val JvmType type = property.typeofLastSegment

			var YField newField = null
			if (typeHelper.isString(type)) {
				newField = ExtensionModelFactory.eINSTANCE.createYTextSearchField
			} else if (typeHelper.isNumber(type)) {
				newField = ExtensionModelFactory.eINSTANCE.createYNumericSearchField
			} else if (typeHelper.isBoolean(type)) {
				newField = ExtensionModelFactory.eINSTANCE.createYBooleanSearchField
			}

			if (newField !== null) {
				layout.elements += newField
			}
		}
	}

	def void createTransient(UiMobileNavigationPage eObject) {
		val VMNavigationPage yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiMobileNavigationPageAssignment eObject) {
		val VMNavigationPage layout = peek
		val element = eObject.element
		val newField = element.create
		layout.addElement(newField)

		if (eObject.alignment !== UiAlignment.UNDEFINED) {
			val style = layout.addCellStyle(newField)
			style.alignment = eObject.alignment.toYAlignment
		}

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}

	}

	def dispatch void map(UiFormLayout eObject) {
		val YFormLayout yField = eObject.associatedUi
		yField.push

		eObject.contents.forEach [
			it.map
		]

		eObject.bindings.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiFormLayoutAssigment eObject) {

		val YFormLayout layout = peek
		val element = eObject.element
		val newField = element.create
		if (newField === null) {
			return
		}
		layout.addElement(newField)

		element.map

		if (element instanceof UiField) {
			newField.push
			val UiField yField = element as UiField
			yField.validators.forEach [
				it.map
			]
			pop
		}

	}

	def dispatch void map(UiOptionsGroup eObject) {
		val YField yOptionsGroup = eObject.associatedUi
		yOptionsGroup.push

		eObject.bindings.forEach [
			it.map
		]

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiList eObject) {
		val YField yList = eObject.associatedUi
		yList.push

		eObject.bindings.forEach [
			it.map
		]

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiTable eObject) {
		val YTable yField = eObject.associatedUi
		yField.push

		if (eObject.columnAssignment !== null) {
			eObject.columnAssignment.columns.forEach [
				it.map
			]
		}

		if (eObject.sortOrderAssignment !== null) {
			eObject.sortOrderAssignment.columns.forEach [
				it.map
			]
		}

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiImage eObject) {
		val YImage yField = eObject.associatedUi
		yField.push

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		pop
	}

	def dispatch void map(UiComboBox eObject) {
		val YField yField = eObject.associatedUi
		yField.push

		if (eObject.bindings !== null) {
			eObject.bindings.forEach [
				it.map
			]
		}

		eObject.processorAssignments.forEach [
			it.map
		]

		pop
	}

	def dispatch void map(UiButton object) {
		object.bindings.forEach [
			it.map
		]

		object.processorAssignments.forEach [
			it.map
		]

	}

	def dispatch void map(UiMobileNavBarAction object) {
		object.bindings.forEach [
			it.map
		]

		object.processorAssignments.forEach [
			it.map
		]
	}

	def dispatch void map(UiMobileNavigationButton object) {
		val VMNavigationButton button = object.associatedUi

		button.push
		if (object.targetPage !== null) {
			val page = object.targetPage.create as VMNavigationPage
			button.page = page
			object.targetPage.map
		} else if (object.targetPageAlias !== null) {
			object.targetPageAlias.map
		}

		object.bindings.forEach [
			it.map
		]

		object.processorAssignments.forEach [
			it.map
		]

		pop

	}

	def dispatch void map(UiColumn eObject) {
		val YTable yField = peek

		val YColumn yColumn = factory.createColumn
		yColumn.alignment = eObject.alignment.toYFlatAlignment
		yColumn.collapsed = eObject.collapsed
		yColumn.collapsible = eObject.collapsible
		yColumn.expandRatio = eObject.expandRatio
		yColumn.icon = eObject.iconName

		val UiNestedProperty property = eObject.property
		if (property !== null) {
			yColumn.propertyPath = property.toPathString
		}

		yColumn.orderable = eObject.orderable
		yColumn.visible = eObject.visible
		yColumn.labelI18nKey = eObject.toI18nKey
		// removed for while
//		if (yColumn.icon === null || yColumn.icon.equals("")) {
//			yColumn.icon = eObject.toI18nKey + ".image"
//		}

		// get UiTable
		var uiTable = eObject.eContainer
		while (uiTable !== null && !(uiTable instanceof UiTable)){
			uiTable = uiTable.eContainer
		}
		if (uiTable !== null){
			var uiTableType = (uiTable as UiTable).jvmType.type
			if (uiTableType instanceof JvmDeclaredType) {
				// get type of the concerning attribute
				var attributeType = (uiTableType as JvmDeclaredType).toFieldTypeByName(yColumn.propertyPath)
				if (attributeType !== null) {
					yColumn.type = loadClass(eObject.eResource.resourceSet, attributeType)
					yColumn.typeQualifiedName = attributeType
				}
				// set attribute properties
				for (pair : (uiTableType as JvmDeclaredType).toProperties(yColumn.propertyPath)) {
					yColumn.properties.put(pair.key, pair.value)
				}
			}
		}
		yField.columns += yColumn
	}

	 /**
	 * Detects all the existing properties of the jvmtype corresponding attribute, the datatype of the attribute and references. 
	 */
	private def List<Pair> toProperties(JvmDeclaredType type, String attributeName) {
		val result = type.toFieldPropertiesByName(attributeName)
		if (result !== null) {
			for (prop : result) {

				// unescape quotes like &curren; to avoid UTF-8 conflicts
				prop.value = StringEscapeUtils.unescapeHtml(prop.value);
			}
		}
		return if(result !== null) result else newArrayList()

	}
	
	static class Pair {
		public String key
		public String value

		new(String key, String value) {
			this.key = key;
			this.value = value;
		}

	}
	
	
	/**
	 * Detects the qualified name of the jvmtype corresponding reference and the referenced dto. 
	 */
	private def String toFieldTypeByName(JvmDeclaredType jvmType, String attributeName) {
		val List<String> result = newArrayList
		// In case of a dot noted attribute name (due to a reference)
		if (attributeName.contains(".")){
			jvmType.findAllFeaturesByName(attributeName.substring(0,attributeName.indexOf("."))).filter(typeof(JvmField)).forEach [
				result.add(toFieldTypeByName(it.type.type as JvmDeclaredType, attributeName.substring(attributeName.indexOf(".") + 1)))
			]
		// Otherwise
		} else {
			jvmType.findAllFeaturesByName(attributeName).filter(typeof(JvmField)).forEach [
				result.add(it.type.type.qualifiedName)
			]
		}
		return if (result.size > 0) result.get(0) else null
	}
	
	/**
	 * Detects all the existing properties of the jvmtype corresponding reference and the referenced dto. 
	 */
	private def List<Pair> toFieldPropertiesByName(JvmDeclaredType jvmType, String attributeName) {
		val List<Pair> values = newArrayList()
		jvmType.findAllFeaturesByName(attributeName).filter(typeof(JvmField)).forEach [
			it.annotations.filter [ e |
				val result = e.annotation.identifier.equals(typeof(Properties).name)
				return result
			].forEach [
				val JvmAnnotationReference ref = it;
				ref.values.map[it as JvmAnnotationAnnotationValue].forEach [ av |
					av.values.forEach [
						val keyAn = it.values.get(0) as JvmStringAnnotationValue
						val valueAn = it.values.get(1) as JvmStringAnnotationValue
						values.add(new Pair(keyAn.values.get(0), valueAn.values.get(0)))
					]
				]
			]
		]

		return values
	}
	
	def dispatch void map(UiSortOrder eObject) {
		val YTable yField = peek

		val YSortColumn yColumn = factory.createSortColumn

		yColumn.asc = eObject.asc

		val UiNestedProperty property = eObject.property
		if (property !== null) {
			yColumn.propertyPath = property.toPathString
		}

		yField.sortOrder += yColumn
	}

	def YDateTimeFormat toYDateTimeFormat(UiDateFormat format) {
		switch (format) {
			case DATE:
				return YDateTimeFormat.DATE
			case DATE_TIME:
				return YDateTimeFormat.DATE_TIME
			case TIME:
				return YDateTimeFormat.TIME
		}
	}

	def YDateTimeResolution toYDateTimeResolution(UiDateTimeResolution value) {
		switch (value) {
			case SECOND:
				return YDateTimeResolution.SECOND
			case MINUTE:
				return YDateTimeResolution.MINUTE
			case HOUR:
				return YDateTimeResolution.HOUR
			case DAY:
				return YDateTimeResolution.DAY
			case MONTH:
				return YDateTimeResolution.MONTH
			case YEAR:
				return YDateTimeResolution.YEAR
			case UNDEFINED:
				return YDateTimeResolution.UNDEFINED
		}
	}

	def YFlatAlignment toYFlatAlignment(UiFlatAlignment uiAlign) {
		switch (uiAlign) {
			case LEFT:
				return YFlatAlignment.LEFT
			case CENTER:
				return YFlatAlignment.CENTER
			case RIGHT:
				return YFlatAlignment.RIGHT
		}
	}

	def YAlignment toYAlignment(UiAlignment uiAlign) {
		switch (uiAlign) {
			case BOTTOM_CENTER:
				return YAlignment.BOTTOM_CENTER
			case BOTTOM_FILL:
				return YAlignment.BOTTOM_FILL
			case BOTTOM_LEFT:
				return YAlignment.BOTTOM_LEFT
			case BOTTOM_RIGHT:
				return YAlignment.BOTTOM_RIGHT
			case FILL_CENTER:
				return YAlignment.FILL_CENTER
			case FILL_FILL:
				return YAlignment.FILL_FILL
			case FILL_LEFT:
				return YAlignment.FILL_LEFT
			case FILL_RIGHT:
				return YAlignment.FILL_RIGHT
			case MIDDLE_CENTER:
				return YAlignment.MIDDLE_CENTER
			case MIDDLE_FILL:
				return YAlignment.MIDDLE_FILL
			case MIDDLE_LEFT:
				return YAlignment.MIDDLE_LEFT
			case MIDDLE_RIGHT:
				return YAlignment.MIDDLE_RIGHT
			case TOP_CENTER:
				return YAlignment.TOP_CENTER
			case TOP_FILL:
				return YAlignment.TOP_FILL
			case TOP_LEFT:
				return YAlignment.TOP_LEFT
			case TOP_RIGHT:
				return YAlignment.TOP_RIGHT
			case UNDEFINED:
				return YAlignment.UNDEFINED
		}
	}

	def dispatch void map(UiValidatorAssignment eObject) {

		val YField yField = eObject.field.associatedUi
		yField.push

		if (eObject.validatorAlias !== null) {
			eObject.validatorAlias.map
		} else if (eObject.validatorDef !== null) {
			eObject.validatorDef.map
		}
		pop
	}

	def dispatch void map(UiExposedAction object) {
		val YExposedAction yAction = CoreModelFactory.eINSTANCE.createYExposedAction
		if (object.actionReference !== null) {
			yAction.id = object.actionReference.name
			yAction.externalCommandId = object.actionReference.externalCommandId
			yAction.initialEnabled = !object.actionReference.initialDisabled
		} else {
			yAction.id = object.actionID
		}
		yAction.name = object.name
		yAction.label = object.name
		yAction.labelI18nKey = object.toI18nKey
		yAction.icon = object.iconName
		if (yAction.icon === null || yAction.icon.equals("")) {
			yAction.icon = object.toI18nKey + ".image"
		}
		yAction.checkDirty = object.checkDirty
		if (object.externalCommandId !== null) {
			yAction.externalCommandId = object.externalCommandId
		}

		object.associateUi(yAction)

		object.bindings.forEach [
			it.map
		]

		currentView.exposedActions += yAction
	}

	def dispatch void map(UiValidatorDef eObject) {
		eObject.validator.map
	}

	def dispatch void map(UiValidatorAlias eObject) {
		if (viewContext.length === 0) {
			return
		}

	// eObject.validator.map
	}

	def dispatch void map(UiVisibilityProcessorAssignment eObject) {

		// must be installed after all UI elements have been created
		pendingVisibilityProcessors += eObject
	}

	def dispatch void map(UiVisibilityProcessor eObject) {
		val YVisibilityProcessor yProcessor = VisibilityFactory.eINSTANCE.createYVisibilityProcessor
		yProcessor.delegate = loadClass(eObject.eResource.resourceSet, eObject.fullyQualifiedName.toString)
		yProcessor.delegateQualifiedName = eObject.fullyQualifiedName.toString
		currentView.visibilityProcessors += yProcessor

		for (trigger : eObject.changeTriggers) {
			val YValueBindingEndpoint sourceEP = (trigger.endpoint as UiBindingEndpointAssignment).
				createValueBindingEndpoint()
			val YVisibilityProcessorValueBindingEndpoint targetEP = BindingFactory.eINSTANCE.
				createYVisibilityProcessorValueBindingEndpoint
			targetEP.property = trigger.alias

			// create a new binding
			val YValueBinding binding = BindingFactory.eINSTANCE.createYValueBinding();
			binding.setTargetEndpoint(targetEP);
			binding.setModelEndpoint(sourceEP);
			binding.setModelToTargetStrategy(YBindingUpdateStrategy.UPDATE);
			binding.setTargetToModelStrategy(YBindingUpdateStrategy.NEVER);
			yProcessor.triggersOn += binding
		}

		for (dataUsed : eObject.dataUsed) {
			val YValueBindingEndpoint sourceEP = (dataUsed.endpoint as UiBindingEndpointAssignment).
				createValueBindingEndpoint()
			val YVisibilityProcessorValueBindingEndpoint targetEP = BindingFactory.eINSTANCE.
				createYVisibilityProcessorValueBindingEndpoint
			targetEP.property = dataUsed.alias

			// create a new binding
			val YValueBinding binding = BindingFactory.eINSTANCE.createYValueBinding();
			binding.setTargetEndpoint(targetEP);
			binding.setModelEndpoint(sourceEP);
			binding.setModelToTargetStrategy(YBindingUpdateStrategy.UPDATE);
			binding.setTargetToModelStrategy(YBindingUpdateStrategy.NEVER);
			yProcessor.dataUsed += binding
		}
	}

	def dispatch void map(UiMaxLengthValidator eObject) {
		val YMaxLengthValidator newValidator = factory.createMaxLengthValidator
		newValidator.maxLength = eObject.maxLength
		newValidator.errorCode = eObject.errorCode?.fullyQualifiedName?.toString
		newValidator.defaultErrorMessage = eObject.errorCode?.defaultMessage
		eObject.associateUi(newValidator)

		val YField yField = peek
		if (yField !== null) {
			yField.validators += newValidator
		}
	}

	def dispatch void map(UiMinLengthValidator eObject) {
		val YMinLengthValidator newValidator = factory.createMinLengthValidator
		newValidator.minLength = eObject.minLength
		newValidator.errorCode = eObject.errorCode?.fullyQualifiedName?.toString
		newValidator.defaultErrorMessage = eObject.errorCode?.defaultMessage

		eObject.associateUi(newValidator)

		val YField yField = peek
		if (yField !== null) {
			yField.validators += newValidator
		}
	}

	def dispatch void map(UiBeanValidationValidator eObject) {
		val YBeanValidationValidator newValidator = factory.createBeanValidationValidator
		newValidator.errorCode = eObject.errorCode?.fullyQualifiedName?.toString
		newValidator.defaultErrorMessage = eObject.errorCode?.defaultMessage
		eObject.associateUi(newValidator)

		val YField yField = peek
		if (yField !== null) {
			yField.validators += newValidator
		}
	}

	def dispatch void map(UiRegexpValidator eObject) {
		val YRegexpValidator newValidator = factory.createRegexpValidator
		newValidator.regExpression = eObject.regExpression
		newValidator.errorCode = eObject.errorCode?.fullyQualifiedName?.toString
		newValidator.defaultErrorMessage = eObject.errorCode?.defaultMessage

		eObject.associateUi(newValidator)

		val YField yField = peek
		if (yField !== null) {
			yField.validators += newValidator
		}
	}

	def dispatch void map(UiXbaseValidator eObject) {

		val jvmTypes = eObject.getJvmElements
		val JvmGenericType jvmType = if(jvmTypes.size > 0) eObject.getJvmElements.get(0) as JvmGenericType

		val YField yField = peek
		if (jvmType !== null && yField !== null) {
			val YClassDelegateValidator yValidator = factory.createClassDelegateValidator
			eObject.associateUi(yValidator)
			yValidator.className = jvmType.qualifiedName
			yField.validators += yValidator
		}
	}

	def dispatch YEmbeddable create(UiEmbeddable object) {
	}

	def dispatch YEmbeddable create(Void object) {
		return null
	}

	def dispatch YEmbeddable create(UiTextField object) {
		val YTextField textField = factory.createTextField
		textField.id = UiModelGrammarUtil.getPathId(object)
		textField.name = object.name
		textField.label = object.name
		textField.labelI18nKey = object.toI18nKey
		textField.initialEnabled = !object.readonly
		textField.cssClass = object.styles

		val dt = factory.createTextDatatype
		textField.datatype = dt
		textField.orphanDatatypes += dt

		dt.maxLength = object.maxLength
		dt.minLength = object.minLength
		dt.regExpression = object.regex
		object.associateUi(textField)

		return textField
	}

	def dispatch YEmbeddable create(UiPasswordField object) {
		val YPasswordField textField = ExtensionModelFactory.eINSTANCE.createYPasswordField
		textField.id = UiModelGrammarUtil.getPathId(object)
		textField.name = object.name
		textField.label = object.name
		textField.labelI18nKey = object.toI18nKey
		textField.initialEnabled = !object.readonly
		textField.cssClass = object.styles

		object.associateUi(textField)

		return textField
	}

	def dispatch YEmbeddable create(UiSuggestTextField object) {
		val YSuggestTextField textField = ExtensionModelFactory.eINSTANCE.createYSuggestTextField
		textField.id = UiModelGrammarUtil.getPathId(object)
		textField.name = object.name
		textField.label = object.name
		textField.labelI18nKey = object.toI18nKey
		textField.initialEnabled = !object.readonly
		textField.cssClass = object.styles
		textField.useSuggestions = true
		textField.autoHidePopup = object.autoHidePopup

		if (object.jvmType !== null) {
			textField.typeQualifiedName = object.jvmType.qualifiedName
			textField.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
		}

		if (object.itemCaptionProperty !== null) {
			textField.itemCaptionProperty = OperationExtensions.toPropertyName(object.itemCaptionProperty?.simpleName)
		} else {
			textField.itemCaptionProperty = BeanHelper.findCaptionProperty(textField.type)
		}
		textField.itemFilterProperty = OperationExtensions.toPropertyName(object.itemFilterProperty?.simpleName)
		textField.itemUUIDProperty = OperationExtensions.toPropertyName(object.itemUUIDProperty?.simpleName)

		object.associateUi(textField)

		return textField
	}

	def dispatch YEmbeddable create(UiBeanReferenceField object) {
		val YBeanReferenceField field = ExtensionModelFactory.eINSTANCE.createYBeanReferenceField
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.useBeanService = object.consumeBeanService
		field.labelI18nKey = object.toI18nKey

		if (object.jvmType !== null) {
			field.typeQualifiedName = object.jvmType.qualifiedName
			field.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
		}

		val UiNestedProperty captionProperty = object.captionProperty
		if (captionProperty !== null) {
			field.captionPropertyPath = captionProperty.toPathString
		} else {
			field.captionPropertyPath = BeanHelper.findCaptionProperty(field.type)
		}

		val UiNestedProperty imageProperty = object.imageProperty
		if (imageProperty !== null) {
			field.imagePropertyPath = imageProperty.toPathString
		}

		if (object.inMemoryBeanProvider !== null) {
			field.inMemoryBeanProviderQualifiedName = object.inMemoryBeanProvider.qualifiedName
			field.inMemoryBeanProvider = loadClass(object.eResource.resourceSet,
				object.inMemoryBeanProvider.qualifiedName)
		}

		if (object.referenceSourceJvmType !== null) {
			field.referenceSourceTypeQualifiedName = object.referenceSourceJvmType.qualifiedName
			field.referenceSourceType = loadClass(object.eResource.resourceSet,
				object.referenceSourceJvmType.qualifiedName)
			field.referenceSourceTypeProperty = object.referenceSourceField?.simpleName
		}

		object.associateUi(field)

		return field
	}

	def dispatch YEmbeddable create(UiLabel object) {
		val YLabel label = factory.createLabel
		label.id = UiModelGrammarUtil.getPathId(object)
		label.name = object.name
		label.label = object.name
		label.labelI18nKey = object.toI18nKey
		label.cssClass = object.styles

		object.associateUi(label)

		return label
	}

	def dispatch YEmbeddable create(UiDecimalField object) {
		val YDecimalField decimalField = factory.createDecimalField
		decimalField.id = UiModelGrammarUtil.getPathId(object)
		decimalField.name = object.name
		decimalField.label = object.name
		decimalField.labelI18nKey = object.toI18nKey
		decimalField.initialEnabled = !object.readonly
		decimalField.cssClass = object.styles

		val dt = factory.createDecimalDatatype
		decimalField.datatype = dt
		decimalField.orphanDatatypes += dt

		dt.grouping = !object.noGrouping
		dt.markNegative = !object.noMarkNegative
		dt.precision = object.precision

		object.associateUi(decimalField)

		return decimalField
	}

	def dispatch YEmbeddable create(UiTextArea object) {
		val YTextArea textArea = factory.createTextArea
		textArea.id = UiModelGrammarUtil.getPathId(object)
		textArea.name = object.name
		textArea.label = object.name
		textArea.labelI18nKey = object.toI18nKey
		textArea.initialEnabled = !object.readonly
		textArea.cssClass = object.styles

		object.associateUi(textArea)

		return textArea
	}

	def dispatch YEmbeddable create(UiRichTextArea object) {
		val YRichTextArea textField = YECviewFactory.eINSTANCE.createYRichTextArea
		textField.id = UiModelGrammarUtil.getPathId(object)
		textField.name = object.name
		textField.label = object.name
		textField.labelI18nKey = object.toI18nKey
		textField.initialEnabled = !object.readonly
		textField.cssClass = object.styles
		textField.useBlob = object.asBlob

		object.associateUi(textField)

		return textField
	}

	def dispatch YEmbeddable create(UiOptionsGroup object) {

		if (object.jvmType?.type instanceof JvmEnumerationType) {
			val YEnumOptionsGroup optionsGroup = ExtensionModelFactory.eINSTANCE.createYEnumOptionsGroup
			optionsGroup.id = UiModelGrammarUtil.getPathId(object)
			optionsGroup.name = object.name
			optionsGroup.label = object.name
			optionsGroup.labelI18nKey = object.toI18nKey
			optionsGroup.selectionType = object.selectionType.convert
			optionsGroup.initialEnabled = !object.readonly
			optionsGroup.cssClass = object.styles

			if (object.jvmType !== null) {
				optionsGroup.typeQualifiedName = object.jvmType.qualifiedName
				optionsGroup.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}

			object.associateUi(optionsGroup)

			return optionsGroup
		} else {
			val YOptionsGroup optionsGroup = factory.createOptionsGroup
			optionsGroup.id = UiModelGrammarUtil.getPathId(object)
			optionsGroup.name = object.name
			optionsGroup.label = object.name
			optionsGroup.useBeanService = object.consumeBeanService
			optionsGroup.labelI18nKey = object.toI18nKey
			optionsGroup.selectionType = object.selectionType.convert
			optionsGroup.initialEnabled = !object.readonly

			if (object.jvmType !== null) {
				optionsGroup.typeQualifiedName = object.jvmType.qualifiedName
				optionsGroup.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}
			if (object.itemCaptionProperty !== null) {
				optionsGroup.captionProperty = OperationExtensions.toPropertyName(
					object.itemCaptionProperty?.simpleName)
			} else {
				optionsGroup.captionProperty = BeanHelper.findCaptionProperty(optionsGroup.type)
			}
			optionsGroup.imageProperty = OperationExtensions.toPropertyName(object.itemImageProperty?.simpleName)

			object.associateUi(optionsGroup)

			return optionsGroup
		}

	}

	def dispatch YEmbeddable create(UiList object) {
		if (object.jvmType?.type instanceof JvmEnumerationType) {
			val YEnumList list = ExtensionModelFactory.eINSTANCE.createYEnumList
			list.id = UiModelGrammarUtil.getPathId(object)
			list.name = object.name
			list.label = object.name
			list.labelI18nKey = object.toI18nKey
			list.selectionType = object.selectionType.convert
			list.initialEnabled = !object.readonly
			list.cssClass = object.styles

			if (object.jvmType !== null) {
				list.typeQualifiedName = object.jvmType.qualifiedName
				list.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}

			object.associateUi(list)

			return list
		} else {
			val YList list = factory.createList
			list.id = UiModelGrammarUtil.getPathId(object)
			list.name = object.name
			list.label = object.name
			list.useBeanService = object.consumeBeanService
			list.labelI18nKey = object.toI18nKey
			list.selectionType = object.selectionType.convert
			list.initialEnabled = !object.readonly

			if (object.jvmType !== null) {
				list.typeQualifiedName = object.jvmType.qualifiedName
				list.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}
			if (object.itemCaptionProperty !== null) {
				list.captionProperty = OperationExtensions.toPropertyName(object.itemCaptionProperty?.simpleName)
			} else {
				list.captionProperty = BeanHelper.findCaptionProperty(list.type)
			}
			list.imageProperty = OperationExtensions.toPropertyName(object.itemImageProperty?.simpleName)

			object.associateUi(list)

			return list
		}
	}

	def dispatch YEmbeddable create(UiDateField object) {
		val YDateTime dateTime = factory.createDateTime
		dateTime.id = UiModelGrammarUtil.getPathId(object)
		dateTime.name = object.name
		dateTime.label = object.name
		dateTime.labelI18nKey = object.toI18nKey
		dateTime.dateFormat = object.dateFormat.toYDateTimeFormat
		dateTime.resolution = object.resolution.toYDateTimeResolution
		dateTime.initialEnabled = !object.readonly
		dateTime.cssClass = object.styles

		object.associateUi(dateTime)

		return dateTime
	}

	def dispatch YEmbeddable create(UiBrowser object) {
		val YBrowser browser = factory.createBrowser
		browser.id = UiModelGrammarUtil.getPathId(object)
		browser.name = object.name
		browser.label = object.name
		browser.labelI18nKey = object.toI18nKey
		browser.initialEnabled = !object.readonly
		browser.cssClass = object.styles

		object.associateUi(browser)

		return browser
	}

	def dispatch YEmbeddable create(UiProgressBar object) {
		val YProgressBar progressBar = factory.createProgressBar
		progressBar.id = UiModelGrammarUtil.getPathId(object)
		progressBar.name = object.name
		progressBar.label = object.name
		progressBar.labelI18nKey = object.toI18nKey
		progressBar.cssClass = object.styles

		object.associateUi(progressBar)

		return progressBar
	}

	def dispatch YEmbeddable create(UiImage object) {
		val YImage image = factory.createImage
		image.id = UiModelGrammarUtil.getPathId(object)
		image.name = object.name
		image.label = object.name
		image.labelI18nKey = object.toI18nKey
		image.cssClass = object.styles

		image.value = object.value

		object.associateUi(image)

		return image
	}

	def dispatch YEmbeddable create(UiTable object) {
		val YTable table = factory.createTable
		table.id = UiModelGrammarUtil.getPathId(object)
		table.name = object.name
		table.label = object.name
		table.useBeanService = object.consumeBeanService
		table.labelI18nKey = object.toI18nKey
		table.selectionType = object.selectionType.convert
		table.initialEnabled = !object.readonly
		table.cssClass = object.styles
		table.scrollToBottom = object.scrollToBottom
		table.pageLength = object.pageLength;

		table.itemImageProperty = OperationExtensions.toPropertyName(object.itemImageProperty?.simpleName)

		if (object.jvmType !== null) {
			table.typeQualifiedName = object.jvmType.qualifiedName
			table.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
		}

		object.associateUi(table)

		return table
	}

	def dispatch YField create(UiSearchField eObject) {
		val UiNestedProperty property = eObject.property
		if (property !== null) {
			val JvmType type = property.typeofLastSegment
			if (type === null) {
				return null
			}

			var YField newField = null
			if (typeHelper.isString(type)) {
				val temp = ExtensionModelFactory.eINSTANCE.createYTextSearchField
				temp.propertyPath = property.toPathString
				newField = temp
			} else if (typeHelper.isNumber(type)) {
				val temp = ExtensionModelFactory.eINSTANCE.createYNumericSearchField
				temp.propertyPath = property.toPathString
				temp.type = typeHelper.toNumericType(type)
				temp.typeQualifiedName = typeHelper.toNumericQualifiedName(type)
				newField = temp
			} else if (typeHelper.isBoolean(type)) {
				val temp = ExtensionModelFactory.eINSTANCE.createYBooleanSearchField
				temp.propertyPath = property.toPathString
				newField = temp
			} else {
				val temp = ExtensionModelFactory.eINSTANCE.createYReferenceSearchField
				temp.propertyPath = property.toPathString
				temp.typeQualifiedName = property.typeofLastSegment?.qualifiedName
				temp.type = loadClass(eObject.eResource.resourceSet, temp.typeQualifiedName)
				newField = temp
			}

			if (newField !== null) {
				newField.id = UiModelGrammarUtil.getPathId(eObject)
				newField.labelI18nKey = eObject.toI18nKey
				newField.initialEnabled = !eObject.readonly
				newField.cssClass = eObject.styles
			}

			return newField
		}
	}

	def YSelectionType convert(UiSelectionType type) {
		switch (type) {
			case NONE:
				return YSelectionType.SINGLE
			case SINGLE:
				return YSelectionType.SINGLE
			case MULTI:
				return YSelectionType.MULTI
		}
	}

	def dispatch YEmbeddable create(UiNumericField object) {
		val YNumericField field = factory.createNumericField
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		val dt = factory.createNumericDatatype
		field.datatype = dt
		field.orphanDatatypes += dt

		dt.grouping = !object.noGrouping
		dt.markNegative = !object.noMarkNegative
		object.associateUi(field)

		return field
	}

	def dispatch YEmbeddable create(UiCheckBox object) {
		val YCheckBox field = factory.createCheckBox
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch YButton create(UiButton object) {
		val YButton field = factory.createButton
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch VMNavigationBarButton create(UiMobileNavBarAction object) {
		val VMNavigationBarButton field = VaadinMobileFactory.eINSTANCE.createVMNavigationBarButton
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch VMNavigationButton create(UiMobileNavigationButton object) {
		val VMNavigationButton field = VaadinMobileFactory.eINSTANCE.createVMNavigationButton
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch VMNavigationPage create(UiMobileNavigationPage object) {
		var VMNavigationPage field = VaadinMobileFactory.eINSTANCE.createVMNavigationPage
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch YEmbeddable create(UiComboBox object) {

		if (object.jvmType?.type instanceof JvmEnumerationType) {
			val YEnumComboBox field = ExtensionModelFactory.eINSTANCE.createYEnumComboBox

			field.id = UiModelGrammarUtil.getPathId(object)
			field.name = object.name
			field.label = object.name
			field.labelI18nKey = object.toI18nKey
			field.initialEnabled = !object.readonly
			field.cssClass = object.styles

			if (object.jvmType !== null) {
				field.typeQualifiedName = object.jvmType.qualifiedName
				field.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}

			object.associateUi(field)

			return field
		} else {
			val YComboBox field = factory.createComboBox
			field.id = UiModelGrammarUtil.getPathId(object)
			field.name = object.name
			field.label = object.name
			field.useBeanService = object.consumeBeanService
			field.labelI18nKey = object.toI18nKey
			field.initialEnabled = !object.readonly

			if (object.jvmType !== null) {
				field.typeQualifiedName = object.jvmType.qualifiedName
				field.type = loadClass(object.eResource.resourceSet, object.jvmType.qualifiedName)
			}

			if (object.itemCaptionProperty !== null) {
				field.captionProperty = OperationExtensions.toPropertyName(object.itemCaptionProperty?.simpleName)
			} else {
				field.captionProperty = BeanHelper.findCaptionProperty(field.type)
			}
			field.imageProperty = OperationExtensions.toPropertyName(object.itemImageProperty?.simpleName)

			object.associateUi(field)
			return field
		}

	}

	def dispatch YEmbeddable create(UiSwitch object) {
		val VMSwitch field = VaadinMobileFactory.eINSTANCE.createVMSwitch
		field.id = UiModelGrammarUtil.getPathId(object)
		field.name = object.name
		field.label = object.name
		field.labelI18nKey = object.toI18nKey
		field.initialEnabled = !object.readonly
		field.cssClass = object.styles

		object.associateUi(field)

		return field
	}

	def dispatch YGridLayout create(UiGridLayout object) {
		val YGridLayout layout = factory.createGridLayout
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.columns = object.columns
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YFormLayout create(UiFormLayout object) {
		val YFormLayout layout = factory.createFormLayout
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YHorizontalLayout create(UiHorizontalLayout object) {
		val YHorizontalLayout layout = factory.createHorizontalLayout
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch VMNavigationRoot create(UiMobileNavigationRoot object) {
		val VMNavigationRoot layout = VaadinMobileFactory.eINSTANCE.createVMNavigationRoot
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YSearchPanel create(UiSearchPanel object) {
		val YSearchPanel layout = ExtensionModelFactory.eINSTANCE.createYSearchPanel
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch VMSearchPanel create(UiMobileSearchPanel object) {
		val VMSearchPanel layout = VaadinMobileFactory.eINSTANCE.createVMSearchPanel
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YSplitPanel create(UiSplitpanel object) {
		val YSplitPanel layout = factory.createSplitPanel
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.splitPosition = object.splitPosition
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YPanel create(UiPanel object) {
		val YPanel layout = factory.createPanel
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YVerticalLayout create(UiVerticalLayout object) {
		val YVerticalLayout layout = factory.createVerticalLayout
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch VMHorizontalButtonGroup create(UiHorizontalButtonGroup object) {
		val VMHorizontalButtonGroup layout = VaadinMobileFactory.eINSTANCE.createVMHorizontalButtonGroup
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch VMVerticalComponentGroup create(UiVerticalComponentGroup object) {
		val VMVerticalComponentGroup layout = VaadinMobileFactory.eINSTANCE.createVMVerticalComponentGroup
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.initialEnabled = !object.readonly
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch YTabSheet create(UiTabSheet object) {
		val YTabSheet layout = factory.createTabSheet
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey
		layout.cssClass = object.styles

		object.associateUi(layout)

		return layout
	}

	def dispatch VMTabSheet create(UiMobileTabSheet object) {
		val VMTabSheet layout = VaadinMobileFactory.eINSTANCE.createVMTabSheet
		layout.id = UiModelGrammarUtil.getPathId(object)
		layout.name = object.name
		layout.label = object.name
		layout.labelI18nKey = object.toI18nKey

		object.associateUi(layout)

		return layout
	}

	def void xxaddToParent(YElement embeddable) {
		val context = peek
		if (context instanceof YLayout) {
			val YLayout layout = context as YLayout
			layout.addElement(embeddable as YEmbeddable)
		} else if (context instanceof YView) {
			val YView yView = context as YView
			yView.content = embeddable as YEmbeddable
		} else if (context instanceof YTab) {
			val YTab yTab = context as YTab
			yTab.embeddable = embeddable as YEmbeddable
		} else if (context instanceof VMTab) {
			val VMTab yTab = context as VMTab
			yTab.embeddable = embeddable as YEmbeddable
		} else if (context instanceof VMNavigationButton) {
			val VMNavigationButton yButton = context as VMNavigationButton
			yButton.page = embeddable as VMNavigationPage
		} else if (context instanceof VMNavigationCommand) {
			val VMNavigationCommand yCommand = context as VMNavigationCommand
			yCommand.targetPage = embeddable as VMNavigationPage
		} else if (context instanceof YOpenDialogCommand) {
			val YOpenDialogCommand yCommand = context as YOpenDialogCommand
			yCommand.dialog = embeddable as YDialog
		} else if (context instanceof YDialog) {
			val YDialog yDialog = context as YDialog
			yDialog.content = embeddable as YEmbeddable
		}
	}

	def dispatch void map(UiPoint object) {
	}

	def dispatch void map(UiBindingEndpointAlias object) {
	}

	def dispatch void map(UiBindingEndpointAssignment object) {
	}

	def dispatch void map(UiBinding object) {
		pendingBindings += object
	}

	def void install(UiBinding object) {
		val UiBindingEndpointAlias sourceAlias = object.sourceAlias as UiBindingEndpointAlias
		val source = object.source
		val UiBindingEndpointAlias targetAlias = object.targetAlias as UiBindingEndpointAlias
		val target = object.target

		var UiBindingEndpointAssignment sourceEndpoint = source as UiBindingEndpointAssignment
		if (sourceAlias !== null) {
			sourceEndpoint = sourceAlias.endpoint as UiBindingEndpointAssignment
		}
		var UiBindingEndpointAssignment targetEndpoint = target as UiBindingEndpointAssignment
		if (targetAlias !== null) {
			targetEndpoint = targetAlias.endpoint as UiBindingEndpointAssignment
		}

		if (!object.listBinding) {
			val sourceResult = sourceEndpoint.createValueBindingEndpoint
			val targetResult = targetEndpoint.createValueBindingEndpoint

			var YBindingUpdateStrategy sourceToTargetStrategy = YBindingUpdateStrategy::UPDATE;
			if (!object.sourceToTarget) {
				sourceToTargetStrategy = YBindingUpdateStrategy::ON_REQUEST
			}

			var YBindingUpdateStrategy targetToSourceStrategy = YBindingUpdateStrategy::UPDATE;
			if (!object.targetToSource) {
				targetToSourceStrategy = YBindingUpdateStrategy::ON_REQUEST
			}

			currentView.orCreateBindingSet.addBinding(targetResult, sourceResult, targetToSourceStrategy,
				sourceToTargetStrategy)

			finishValidator(sourceEndpoint, sourceResult, targetEndpoint, targetResult)

		} else {
			val sourceResult = sourceEndpoint.createListBindingEndpoint
			val targetResult = targetEndpoint.createListBindingEndpoint

			var YBindingUpdateStrategy sourceToTargetStrategy = YBindingUpdateStrategy::UPDATE;
			if (!object.sourceToTarget) {
				sourceToTargetStrategy = YBindingUpdateStrategy::ON_REQUEST
			}

			var YBindingUpdateStrategy targetToSourceStrategy = YBindingUpdateStrategy::UPDATE;
			if (!object.targetToSource) {
				targetToSourceStrategy = YBindingUpdateStrategy::ON_REQUEST
			}

			currentView.orCreateBindingSet.addBinding(targetResult, sourceResult, targetToSourceStrategy,
				sourceToTargetStrategy)
		}
	}

	def finishValidator(UiBindingEndpointAssignment sourceAssignment, YValueBindingEndpoint sourceEndpoint,
		UiBindingEndpointAssignment targetAssignment, YValueBindingEndpoint targetEndpoint) {
		val BindingInfoHelper.BindingInfo sourceInfo = new BindingInfoHelper.BindingInfo
		bindingInfoHelper.collectBindingInfo(sourceAssignment, sourceInfo);

		val BindingInfoHelper.BindingInfo targetInfo = new BindingInfoHelper.BindingInfo
		bindingInfoHelper.collectBindingInfo(targetAssignment, targetInfo);

		var JvmType type = null
		var String property = null
		var YField field = null

		if (sourceInfo.bindingRoot instanceof UiField) {
			field = associatedUi(sourceInfo.bindingRoot) as YField;
		} else if (targetInfo.bindingRoot instanceof UiField) {
			field = associatedUi(targetInfo.bindingRoot) as YField;
		}

		if (field !== null) {
			if (sourceInfo.deepestJvmField !== null) {
				val jvmField = sourceInfo.deepestJvmField
				if (jvmField !== null) {
					type = jvmField.declaringType
					property = OperationExtensions.toPropertyName(jvmField.simpleName)
				}
			} else if (targetInfo.deepestJvmField !== null) {
				val jvmField = sourceInfo.deepestJvmField
				if (jvmField !== null) {
					type = jvmField.declaringType
					property = OperationExtensions.toPropertyName(jvmField.simpleName)
				}
			}
		}

		if (type !== null && property !== null && field !== null) {
			for (yValidator : field.validators) {
				yValidator.type = loadClass(sourceAssignment.eResource.resourceSet, type.qualifiedName)

				if (yValidator instanceof YBeanValidationValidator) {
					yValidator.bvalClass = yValidator.type
					yValidator.bvalClassFullyQualifiedName = type.qualifiedName
					yValidator.bvalProperty = property
				}
			}
		}
	}

	def YValueBindingEndpoint createValueBindingEndpoint(UiBindingEndpointAssignment epDef) {
		if (epDef === null) {
			return null
		}
		var YValueBindingEndpoint result = null;
		val BindingInfoHelper.BindingInfo info = new BindingInfoHelper.BindingInfo
		bindingInfoHelper.collectBindingInfo(epDef, info);

		if (info.bindingRoot instanceof UiBeanSlot) {
			val uiBeanSlot = info.bindingRoot as UiBeanSlot

			val YBeanSlot yBeanSlot = uiBeanSlot.associatedUi
			val YBeanSlotValueBindingEndpoint ep = factory.createBeanSlotValueBindingEndpoint
			ep.beanSlot = yBeanSlot
			ep.attributePath = info.path.toString
			if (ep.attributePath === null || ep.attributePath.equals("")) {

				// bind the value in the slot
				ep.attributePath = "value"
			}
			result = ep
		} else if (info.bindingRoot instanceof UiEmbeddable) {
			val YElement yElement = info.bindingRoot.associatedUi
			val YECViewModelValueBindingEndpoint ep = factory.createECViewModelValueBindingEndpoint
			ep.element = yElement
			ep.propertyPath = info.path.toString
			if (info.typeForBinding !== null) {
				ep.typeQualifiedName = info.typeForBinding.qualifiedName
				ep.type = loadClass(epDef.eResource.resourceSet, ep.typeQualifiedName)
			}
			if (ep.type !== null && typeof(EObject).isAssignableFrom(ep.type)) {
				val classifier = findEClassifierFor(epDef.eResource.resourceSet, ep.type.simpleName)
				ep.emfNsURI = if(classifier !== null) classifier.EPackage.nsURI else null
			}

			// calculate default I18nKey
			epDef.eContainer

			result = ep
		} else if (info.bindingRoot instanceof UiMobileNavigationCommand) {
			val UiMobileNavigationCommand command = info.bindingRoot as UiMobileNavigationCommand

			// Create the command and register it at the current view
			val VMNavigationCommand yCommand = VaadinMobileFactory.eINSTANCE.createVMNavigationCommand

			currentView.commandSet.addCommand(yCommand)

			// create the target page and add it to the command
			yCommand.push

			command.targetPage.create
			command.targetPage.map

			yCommand.targetPage = command.targetPage.associatedUi

			// since navHandler is parent, it must be created yet
			val UiMobileNavigationHandler navHandler = epDef.findNavHandler
			yCommand.navigationHandler = (navHandler as EObject).associatedUi

			pop

			result = yCommand.createNavigationValueEndpoint
		} else if (info.bindingRoot instanceof UiOpenDialogCommand) {
			val UiOpenDialogCommand command = info.bindingRoot as UiOpenDialogCommand

			// Create the command and register it at the current view
			val YOpenDialogCommand yCommand = CoreModelFactory.eINSTANCE.createYOpenDialogCommand
			currentView.commandSet.addCommand(yCommand)

			// create the target page and add it to the command
			yCommand.push
			command.dialog.map

			pop

			result = yCommand.createTriggerDialogEndpoint
		} else if (info.bindingRoot instanceof UiSearchWithDialogCommand) {
			val UiSearchWithDialogCommand command = info.bindingRoot as UiSearchWithDialogCommand

			// Create the command and register it at the current view
			val YOpenDialogCommand yCommand = CoreModelFactory.eINSTANCE.createYOpenDialogCommand
			currentView.commandSet.addCommand(yCommand)

			// create the target page and add it to the command
			yCommand.push
			command.dialog.map

			pop

			result = yCommand.createTriggerDialogEndpoint
		} else if (info.bindingRoot instanceof UiAddToTableCommand) {
			val UiAddToTableCommand command = info.bindingRoot as UiAddToTableCommand

			// Create the command and register it at the current view
			val YAddToTableCommand yCommand = ExtensionModelFactory.eINSTANCE.createYAddToTableCommand
			currentView.commandSet.addCommand(yCommand)

			pendingMappings += [
				yCommand.table = command.table.associatedUi
			] as Runnable

			result = yCommand.createTriggerEndpoint
		} else if (info.bindingRoot instanceof UiRemoveFromTableCommand) {
			val UiRemoveFromTableCommand command = info.bindingRoot as UiRemoveFromTableCommand

			// Create the command and register it at the current view
			val YRemoveFromTableCommand yCommand = ExtensionModelFactory.eINSTANCE.createYRemoveFromTableCommand
			currentView.commandSet.addCommand(yCommand)

			pendingMappings += [
				yCommand.table = command.table.associatedUi
			] as Runnable

			result = yCommand.createTriggerEndpoint
		} else if (info.bindingRoot instanceof UiSendEventCommand) {
			val UiSendEventCommand command = info.bindingRoot as UiSendEventCommand

			// Create the command and register it at the current view
			val YSendEventCommand yCommand = CoreModelFactory.eINSTANCE.createYSendEventCommand
			yCommand.autoTrigger = !command.noAutoTrigger
			yCommand.eventTopic = command.eventTopic

			currentView.commandSet.addCommand(yCommand)

			result = yCommand.createMessageEndpoint
		} else if (info.bindingRoot instanceof UiSetNewInstanceCommand) {
			val UiSetNewInstanceCommand command = info.bindingRoot as UiSetNewInstanceCommand

			// Create the command and register it at the current view
			val YSetNewBeanInstanceCommand yCommand = ExtensionModelFactory.eINSTANCE.createYSetNewBeanInstanceCommand
			currentView.commandSet.addCommand(yCommand)

			val targetEP = command.target as UiBindingEndpointAssignment

			yCommand.target = targetEP.createValueBindingEndpoint
			if (command.jvmType !== null) {

				yCommand.typeQualifiedName = command.jvmType.qualifiedName
				yCommand.type = loadClass(command.eResource.resourceSet, command.jvmType.qualifiedName)
			} else {
				val BindingInfoHelper.BindingInfo targetInfo = new BindingInfoHelper.BindingInfo
				bindingInfoHelper.collectBindingInfo(targetEP, targetInfo);

				if (targetInfo.typeOfBoundProperty !== null) {

					// if there is a property path
					yCommand.typeQualifiedName = targetInfo.typeOfBoundProperty.qualifiedName
					yCommand.type = loadClass(epDef.eResource.resourceSet, yCommand.typeQualifiedName)
				} else if (targetInfo.typeForBinding !== null) {

					// use the root object
					yCommand.typeQualifiedName = targetInfo.typeForBinding.qualifiedName
					yCommand.type = loadClass(epDef.eResource.resourceSet, yCommand.typeQualifiedName)
				}
				if (yCommand.type !== null && yCommand.type.isAssignableFrom(typeof(EObject))) {
					// TODO later for EObjects
				}
			}

			result = yCommand.createTriggerEndpoint
		}

		return result
	}

	/**
	 * Attention: If EClasses not named uniquely, this method may return the wrong.<br>
	 * The nsURI from the classifier is required for databinding to find the proper EClass and EFeature for ep.type.
	 */
	def findEClassifierFor(ResourceSet resourceSet, String eClassName) {
		val Set<EClassifier> matches = newHashSet()
		for (String nsUri : new HashSet(EPackage.Registry.INSTANCE.keySet())) {
			try {
				val pkg = EPackage.Registry.INSTANCE.getEPackage(nsUri)
				val classifier = pkg.getEClassifier(eClassName)
				if (classifier !== null) {
					matches += classifier
				}
			} catch (Exception ex) {
				// nothing to do - smooks throws exception if no classifier found	
			}
		}
		if (matches.empty) {
			LOGGER.error("Could not find classifier for name %s", eClassName)
		} else if (matches.size > 1) {
			LOGGER.error("Found %s classifiers for name %s", Integer.toString(matches.size), eClassName)
		}
		return if(matches.isEmpty) return null else matches.iterator.next
	}

	def UiMobileNavigationHandler findNavHandler(UiBindingEndpointAssignment assignment) {
		var EObject temp = assignment;
		while (temp.eContainer !== null) {
			temp = temp.eContainer
			if (temp instanceof UiMobileNavigationHandler) {
				return temp as UiMobileNavigationHandler
			}
		}
		return null
	}

	def YListBindingEndpoint createListBindingEndpoint(UiBindingEndpointAssignment epDef) {
		if (epDef === null) {
			return null
		}

		var YListBindingEndpoint result = null;
		val BindingInfoHelper.BindingInfo info = new BindingInfoHelper.BindingInfo()
		bindingInfoHelper.collectBindingInfo(epDef, info);

		if (info.bindingRoot instanceof UiBeanSlot) {
			val uiBeanSlot = info.bindingRoot as UiBeanSlot

			val YBeanSlot yBeanSlot = uiBeanSlot.associatedUi
			val YBeanSlotListBindingEndpoint ep = factory.createBeanSlotListBindingEndpoint
			ep.beanSlot = yBeanSlot
			ep.attributePath = info.path.toString
			result = ep
		} else if (info.bindingRoot instanceof UiEmbeddable) {
			val YEmbeddable yElement = info.bindingRoot.associatedUi
			val YECViewModelListBindingEndpoint ep = factory.createECViewModelListBindingEndpoint
			ep.element = yElement
			ep.propertyPath = info.path.toString
			if (info.typeForBinding !== null) {
				ep.typeQualifiedName = info.typeForBinding.qualifiedName
				ep.type = loadClass(epDef.eResource.resourceSet, ep.typeQualifiedName)
			}
			if (yElement !== null) {
				ep.emfNsURI = yElement.eClass.EPackage.nsURI
			}
			result = ep
		}

		return result
	}

	def resolve(UiBindingEndpointAlias alias) {
		val temp = alias.endpoint;
		if (temp instanceof UiBindingEndpointAssignment) {
			val UiBindingEndpointAssignment assign = temp as UiBindingEndpointAssignment
			if (assign.typedBindableAlias !== null && assign.typedBindableAlias instanceof UiBindingEndpointAlias) {
				resolve(assign.typedBindableAlias as UiBindingEndpointAlias)
			}
		}
	}

	def Class<?> loadClass(ResourceSet resourceSet, String qualifiedName) {
		return typeLoader.findTypeByName(qualifiedName)
	}

	def dispatch void map(UiPathSegment object) {
	}

	def dispatch void map(UiBeanSlot object) {
		val yBeanSlot = factory.createBeanSlot
		yBeanSlot.name = object.name
		yBeanSlot.valueTypeQualifiedName = object.jvmType?.qualifiedName
		yBeanSlot.valueType = loadClass(object.eResource.resourceSet, yBeanSlot.valueTypeQualifiedName)
		yBeanSlot.eventTopic = object.eventTopic

		object.associateUi(yBeanSlot)

		val EObject lastElement = viewContext.peek
		if (lastElement instanceof YView) {
			(lastElement as YView).beanSlots += yBeanSlot
		} else if (lastElement instanceof YViewSet) {
			(lastElement as YViewSet).beanSlots += yBeanSlot
		}

	}

	def dispatch void map(EObject object) {
	}

	override void discardDerivedState(DerivedStateAwareResource resource) {
		super.discardDerivedState(resource)

		if (resource.contents.size > 1) {
			for (var i = 1; i < resource.contents.size; i++) {
				resource.contents.remove(1)
			}
		}
	}
}
