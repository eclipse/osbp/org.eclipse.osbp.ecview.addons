/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl;

import org.eclipse.osbp.ecview.dsl.derivedstate.UiModelDerivedStateComputerx;
import org.eclipse.osbp.ecview.dsl.generator.OutputConfigurationProvider;
import org.eclipse.osbp.ecview.dsl.imports.ShouldImportProvider;
import org.eclipse.osbp.ecview.dsl.name.UiQualifiedNameProvider;
import org.eclipse.osbp.ecview.dsl.parser.UiGrammarParserCustom;
import org.eclipse.osbp.ecview.dsl.resource.ResourceDescriptionStrategy;
import org.eclipse.osbp.ecview.dsl.resource.persistence.FilteringResourceStorageFacade;
import org.eclipse.osbp.ecview.dsl.scope.ScopeProvider;
import org.eclipse.osbp.ecview.dsl.scope.ScopingInfoProvider;
import org.eclipse.osbp.ecview.dsl.scope.eobject.UiModelEObjectImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.ecview.dsl.scope.jvmtype.UiModelJvmTypesImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.xtext.oxtype.imports.IShouldImportProvider;
import org.eclipse.osbp.xtext.oxtype.imports.OXTypeRewritableImportSection;
import org.eclipse.osbp.xtext.oxtype.linker.JvmTypeAwareLinker;
import org.eclipse.osbp.xtext.oxtype.linking.OXTypeLinkingService;
import org.eclipse.osbp.xtext.oxtype.scoping.IScopingInfoProvider;
import org.eclipse.osbp.xtext.oxtype.scoping.OXDelegatingNamespaceScopeProvider;
import org.eclipse.osbp.xtext.oxtype.scoping.eobject.OXEObjectImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.xtext.oxtype.scoping.jvmtype.OXJvmTypesImportSectionNamespaceScopeProvider;
import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.xbase.imports.RewritableImportSection;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.scoping.batch.IBatchScopeProvider;
import org.eclipse.xtext.xbase.scoping.batch.XbaseBatchScopeProvider;

import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
@SuppressWarnings("restriction")
public class UIGrammarRuntimeModule extends org.eclipse.osbp.ecview.dsl.AbstractUIGrammarRuntimeModule {

	public Class<? extends IParser> bindIParser() {
		return UiGrammarParserCustom.class;
	}

	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return FilteringResourceStorageFacade.class;
	}

	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}

	public Class<? extends org.eclipse.xtext.resource.IDerivedStateComputer> bindIDerivedStateComputer() {
		return UiModelDerivedStateComputerx.class;
	}

	@Override
	public Class<? extends IScopeProvider> bindIScopeProvider() {
		return ScopeProvider.class;
	}

	@Override
	public Class<? extends IBatchScopeProvider> bindIBatchScopeProvider() {
		return ScopeProvider.class;
	}

	@Override
	public Class<? extends XbaseBatchScopeProvider> bindXbaseBatchScopeProvider() {
		return ScopeProvider.class;
	}

	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class)
				.annotatedWith(Names.named("org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider.delegate"))
				.to(OXDelegatingNamespaceScopeProvider.class);
	}

	public Class<? extends org.eclipse.xtext.naming.IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return UiQualifiedNameProvider.class;
	}

	public Class<? extends org.eclipse.xtext.formatting.IFormatter> bindIFormatter() {
		return org.eclipse.osbp.ecview.dsl.formatting.UIGrammarFormatter.class;
	}

	public Class<? extends org.eclipse.xtext.generator.IGenerator> bindIGenerator() {
		return org.eclipse.osbp.ecview.dsl.generator.Generator.class;
	}

	public Class<? extends IOutputConfigurationProvider> bindIOutputConfigurationProvider() {
		return OutputConfigurationProvider.class;
	}

	public Class<? extends org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return ResourceDescriptionStrategy.class;
	}

	public Class<? extends org.eclipse.xtext.linking.ILinker> bindILinker() {
		return JvmTypeAwareLinker.class;
	}

	public Class<? extends ILinkingService> bindILinkingService() {
		return OXTypeLinkingService.class;
	}

	public Class<? extends RewritableImportSection.Factory> bindRewritableImportSection$Factory() {
		return OXTypeRewritableImportSection.Factory.class;
	}

	public Class<? extends IScopingInfoProvider> bindIScopingInfoProvider() {
		return ScopingInfoProvider.class;
	}

	public Class<? extends IShouldImportProvider> bindIShouldImportProvider() {
		return ShouldImportProvider.class;
	}

	public Class<? extends OXJvmTypesImportSectionNamespaceScopeProvider> bindOXJvmTypesImportSectionNamespaceScopeProvider() {
		return UiModelJvmTypesImportSectionNamespaceScopeProvider.class;
	}

	public Class<? extends OXEObjectImportSectionNamespaceScopeProvider> bindOXEObjectImportSectionNamespaceScopeProvider() {
		return UiModelEObjectImportSectionNamespaceScopeProvider.class;
	}

}
