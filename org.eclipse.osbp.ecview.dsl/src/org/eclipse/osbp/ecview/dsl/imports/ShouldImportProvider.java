/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.imports;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;
import org.eclipse.xtext.common.types.JvmType;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	@Override
	public boolean shouldImport(EObject toImport, EReference eRef,
			EObject context) {

		// in this case, we also need to import JvmTypes
		// The JvmTypeOrganizeImport stuff will not do so, since
		// UiBeanSlot#jvmType has no parent context based on JvmType hence no
		// inferrer
		if (toImport instanceof JvmType) {
			return true;
		}

		boolean result = toImport.eResource() != context.eResource();

		if (result == true) {
			return doShouldImport(toImport, eRef, context);
		} else {
			return false;
		}
	}

	protected boolean doShouldImport(EObject toImport, EReference eRef,
			EObject context) {
		return false;
	}
}
