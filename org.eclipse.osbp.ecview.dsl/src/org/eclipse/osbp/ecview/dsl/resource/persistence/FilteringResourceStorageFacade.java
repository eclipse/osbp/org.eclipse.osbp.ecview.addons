/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.resource.persistence;

import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.xtext.oxtype.resource.persistence.FilterResourceStorageFacade;

/**
 * This class ensures, that ECViews are not persisted by Xtext 
 */
public class FilteringResourceStorageFacade extends FilterResourceStorageFacade {

	public FilteringResourceStorageFacade() {
		super((eobject) -> {
			if (eobject instanceof YView) {
				return false;
			}
			return true;
		});
	}
}
