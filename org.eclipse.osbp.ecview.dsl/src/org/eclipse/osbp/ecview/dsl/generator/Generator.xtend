/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.generator

import java.io.StringWriter
import java.io.Writer
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.XMLResource
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl
import org.eclipse.osbp.ecview.core.common.model.core.YView
import org.eclipse.osbp.xtext.oxtype.hooks.DelegatingGenerator
import org.eclipse.xtext.generator.IFileSystemAccess

class Generator extends DelegatingGenerator {

	override doGenerate(Resource input, IFileSystemAccess fsa) {

		super.doGenerate(input, fsa)

		for (YView yView : input.contents.filter(typeof(YView))) {
			val XMLResource outputRes = new XMLResourceImpl
			outputRes.contents += EcoreUtil.copy(yView)

			val Writer writer = new StringWriter
			outputRes.save(writer, null)

			fsa.generateFile(yView.viewName + ".ecview", "ECViews", writer.toString)
		}
	}
}
