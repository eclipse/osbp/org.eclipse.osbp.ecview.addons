/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;
import org.eclipse.osbp.ecview.semantic.uimodel.UiView;

import org.eclipse.xtext.common.types.TypesFactory;

import org.eclipse.xtext.xtype.XtypeFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.semantic.uimodel.UiView} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UiViewItemProvider extends UiContextItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiViewItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addViewSetPropertyDescriptor(object);
			addContentAlignmentPropertyDescriptor(object);
			addViewCategoryPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the View Set feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addViewSetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiView_viewSet_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiView_viewSet_feature", "_UI_UiView_type"),
				 UiModelPackage.Literals.UI_VIEW__VIEW_SET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Content Alignment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContentAlignmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiView_contentAlignment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiView_contentAlignment_feature", "_UI_UiView_type"),
				 UiModelPackage.Literals.UI_VIEW__CONTENT_ALIGNMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the View Category feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addViewCategoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiView_viewCategory_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiView_viewCategory_feature", "_UI_UiView_type"),
				 UiModelPackage.Literals.UI_VIEW__VIEW_CATEGORY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UiModelPackage.Literals.UI_VISIBILITY_PROCESSABLE__PROCESSOR_ASSIGNMENTS);
			childrenFeatures.add(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE);
			childrenFeatures.add(UiModelPackage.Literals.UI_VIEW__CONTENT);
			childrenFeatures.add(UiModelPackage.Literals.UI_VIEW__VALIDATOR_ASSIGNMENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UiView.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UiView"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UiView)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UiView_type") :
			getString("_UI_UiView_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UiView.class)) {
			case UiModelPackage.UI_VIEW__CONTENT_ALIGNMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UiModelPackage.UI_VIEW__PROCESSOR_ASSIGNMENTS:
			case UiModelPackage.UI_VIEW__JVM_TYPE:
			case UiModelPackage.UI_VIEW__CONTENT:
			case UiModelPackage.UI_VIEW__VALIDATOR_ASSIGNMENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VISIBILITY_PROCESSABLE__PROCESSOR_ASSIGNMENTS,
				 UiModelFactory.eINSTANCE.createUiVisibilityProcessorAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmParameterizedTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmGenericArrayTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmWildcardTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmAnyTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmMultiTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmDelegateTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmSynonymTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmUnknownTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmInnerTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 XtypeFactory.eINSTANCE.createXFunctionTypeRef()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 XtypeFactory.eINSTANCE.createXComputedTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiExposedAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiDialog()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiImage()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiList()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiTable()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiColumn()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSortOrderAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSortOrder()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiColumnsAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiHorizontalButtonGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiButton()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSwitch()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiVerticalComponentGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileTabAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationPage()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationButton()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSearchDialog()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiLabel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiDateField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiSplitpanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationRoot()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavBarAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiPrefixedMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMaskedNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiMaskedDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__CONTENT,
				 UiModelFactory.eINSTANCE.createUiRichTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_VIEW__VALIDATOR_ASSIGNMENTS,
				 UiModelFactory.eINSTANCE.createUiValidatorAssignment()));
	}

}
