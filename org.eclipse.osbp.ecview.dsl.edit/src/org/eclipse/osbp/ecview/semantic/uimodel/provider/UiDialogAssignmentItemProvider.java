/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.dsl.provider.UIGrammarEditPlugin;

import org.eclipse.osbp.ecview.semantic.uimodel.UiDialogAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.semantic.uimodel.UiDialogAssignment} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UiDialogAssignmentItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiDialogAssignmentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiNamedElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiNamedElement_id_feature", "_UI_UiNamedElement_type"),
				 UiModelPackage.Literals.UI_NAMED_ELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiNamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiNamedElement_name_feature", "_UI_UiNamedElement_type"),
				 UiModelPackage.Literals.UI_NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UiDialogAssignment.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UiDialogAssignment"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UiDialogAssignment)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UiDialogAssignment_type") :
			getString("_UI_UiDialogAssignment_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UiDialogAssignment.class)) {
			case UiModelPackage.UI_DIALOG_ASSIGNMENT__ID:
			case UiModelPackage.UI_DIALOG_ASSIGNMENT__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UiModelPackage.UI_DIALOG_ASSIGNMENT__ELEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiExposedAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiDialog()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiImage()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiList()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiTable()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiColumn()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSortOrderAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSortOrder()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiColumnsAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiHorizontalButtonGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiButton()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSwitch()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiVerticalComponentGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileTabAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationPage()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationButton()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSearchDialog()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiLabel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiDateField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiSplitpanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavigationRoot()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMobileNavBarAction()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiPrefixedMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMaskedNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiMaskedDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_DIALOG_ASSIGNMENT__ELEMENT,
				 UiModelFactory.eINSTANCE.createUiRichTextArea()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return UIGrammarEditPlugin.INSTANCE;
	}

}
