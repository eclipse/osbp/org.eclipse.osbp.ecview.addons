/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uimodel.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelFactory;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModelPackage;

import org.eclipse.xtext.common.types.TypesFactory;

import org.eclipse.xtext.xtype.XtypeFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UiComboBoxItemProvider extends UiVisibilityProcessableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiComboBoxItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addReadonlyPropertyDescriptor(object);
			addInvisiblePropertyDescriptor(object);
			addStylesPropertyDescriptor(object);
			addConsumeBeanServicePropertyDescriptor(object);
			addItemCaptionPropertyPropertyDescriptor(object);
			addItemImagePropertyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiNamedElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiNamedElement_id_feature", "_UI_UiNamedElement_type"),
				 UiModelPackage.Literals.UI_NAMED_ELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiNamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiNamedElement_name_feature", "_UI_UiNamedElement_type"),
				 UiModelPackage.Literals.UI_NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Readonly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReadonlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiEmbeddable_readonly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiEmbeddable_readonly_feature", "_UI_UiEmbeddable_type"),
				 UiModelPackage.Literals.UI_EMBEDDABLE__READONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Invisible feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInvisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiEmbeddable_invisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiEmbeddable_invisible_feature", "_UI_UiEmbeddable_type"),
				 UiModelPackage.Literals.UI_EMBEDDABLE__INVISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Styles feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStylesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiEmbeddable_styles_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiEmbeddable_styles_feature", "_UI_UiEmbeddable_type"),
				 UiModelPackage.Literals.UI_EMBEDDABLE__STYLES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Consume Bean Service feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConsumeBeanServicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiBeanServiceConsumer_consumeBeanService_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiBeanServiceConsumer_consumeBeanService_feature", "_UI_UiBeanServiceConsumer_type"),
				 UiModelPackage.Literals.UI_BEAN_SERVICE_CONSUMER__CONSUME_BEAN_SERVICE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Item Caption Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItemCaptionPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiComboBox_itemCaptionProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiComboBox_itemCaptionProperty_feature", "_UI_UiComboBox_type"),
				 UiModelPackage.Literals.UI_COMBO_BOX__ITEM_CAPTION_PROPERTY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Item Image Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItemImagePropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UiComboBox_itemImageProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UiComboBox_itemImageProperty_feature", "_UI_UiComboBox_type"),
				 UiModelPackage.Literals.UI_COMBO_BOX__ITEM_IMAGE_PROPERTY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UiModelPackage.Literals.UI_I1_8N_INFOABLE__I18N_INFO);
			childrenFeatures.add(UiModelPackage.Literals.UI_EMBEDDABLE__BINDINGS);
			childrenFeatures.add(UiModelPackage.Literals.UI_FIELD__VALIDATORS);
			childrenFeatures.add(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE);
			childrenFeatures.add(UiModelPackage.Literals.UI_COMBO_BOX__DESCRIPTION_PROPERTY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UiComboBox.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UiComboBox"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UiComboBox)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UiComboBox_type") :
			getString("_UI_UiComboBox_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UiComboBox.class)) {
			case UiModelPackage.UI_COMBO_BOX__ID:
			case UiModelPackage.UI_COMBO_BOX__NAME:
			case UiModelPackage.UI_COMBO_BOX__READONLY:
			case UiModelPackage.UI_COMBO_BOX__INVISIBLE:
			case UiModelPackage.UI_COMBO_BOX__STYLES:
			case UiModelPackage.UI_COMBO_BOX__CONSUME_BEAN_SERVICE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UiModelPackage.UI_COMBO_BOX__I18N_INFO:
			case UiModelPackage.UI_COMBO_BOX__BINDINGS:
			case UiModelPackage.UI_COMBO_BOX__VALIDATORS:
			case UiModelPackage.UI_COMBO_BOX__JVM_TYPE:
			case UiModelPackage.UI_COMBO_BOX__DESCRIPTION_PROPERTY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_I1_8N_INFOABLE__I18N_INFO,
				 UiModelFactory.eINSTANCE.createUiI18nInfo()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_EMBEDDABLE__BINDINGS,
				 UiModelFactory.eINSTANCE.createUiBinding()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_FIELD__VALIDATORS,
				 UiModelFactory.eINSTANCE.createUiMaxLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_FIELD__VALIDATORS,
				 UiModelFactory.eINSTANCE.createUiMinLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_FIELD__VALIDATORS,
				 UiModelFactory.eINSTANCE.createUiRegexpValidator()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_FIELD__VALIDATORS,
				 UiModelFactory.eINSTANCE.createUiBeanValidationValidator()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_FIELD__VALIDATORS,
				 UiModelFactory.eINSTANCE.createUiXbaseValidator()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmParameterizedTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmGenericArrayTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmWildcardTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmAnyTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmMultiTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmDelegateTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmSynonymTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmUnknownTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 TypesFactory.eINSTANCE.createJvmInnerTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 XtypeFactory.eINSTANCE.createXFunctionTypeRef()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_TYPE_PROVIDER__JVM_TYPE,
				 XtypeFactory.eINSTANCE.createXComputedTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(UiModelPackage.Literals.UI_COMBO_BOX__DESCRIPTION_PROPERTY,
				 UiModelFactory.eINSTANCE.createUiNestedProperty()));
	}

}
