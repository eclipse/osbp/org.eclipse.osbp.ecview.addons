/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanReferenceField
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanSlot
import org.eclipse.osbp.ecview.semantic.uimodel.UiBinding
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAlias
import org.eclipse.osbp.ecview.semantic.uimodel.UiBrowser
import org.eclipse.osbp.ecview.semantic.uimodel.UiButton
import org.eclipse.osbp.ecview.semantic.uimodel.UiCheckBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiColumn
import org.eclipse.osbp.ecview.semantic.uimodel.UiColumnAssignments
import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiCommand
import org.eclipse.osbp.ecview.semantic.uimodel.UiDateField
import org.eclipse.osbp.ecview.semantic.uimodel.UiDecimalField
import org.eclipse.osbp.ecview.semantic.uimodel.UiDialog
import org.eclipse.osbp.ecview.semantic.uimodel.UiExposedAction
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiI18nInfo
import org.eclipse.osbp.ecview.semantic.uimodel.UiIDEView
import org.eclipse.osbp.ecview.semantic.uimodel.UiImage
import org.eclipse.osbp.ecview.semantic.uimodel.UiLabel
import org.eclipse.osbp.ecview.semantic.uimodel.UiList
import org.eclipse.osbp.ecview.semantic.uimodel.UiMaxLengthValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiMinLengthValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationButton
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPage
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRoot
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileView
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel
import org.eclipse.osbp.ecview.semantic.uimodel.UiNumericField
import org.eclipse.osbp.ecview.semantic.uimodel.UiOptionsGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiProgressBar
import org.eclipse.osbp.ecview.semantic.uimodel.UiRegexpValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchDialog
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchField
import org.eclipse.osbp.ecview.semantic.uimodel.UiSwitch
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiTable
import org.eclipse.osbp.ecview.semantic.uimodel.UiTextArea
import org.eclipse.osbp.ecview.semantic.uimodel.UiTextField
import org.eclipse.osbp.ecview.semantic.uimodel.UiValidator
import org.eclipse.osbp.ecview.semantic.uimodel.UiValidatorAlias
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiView
import org.eclipse.osbp.ecview.semantic.uimodel.UiViewSet
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiXbaseValidator
import org.eclipse.osbp.xtext.oxtype.ui.labeling.OXtypeLabelProvider

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class UIGrammarLabelProvider extends OXtypeLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	def text(UiModel ele) {
		ele.name
	}

	def text(UiIDEView ele) {
		if(ele.name != null) ele.name else "IDE View"
	}

	def text(UiMobileView ele) {
		if(ele.name != null) ele.name else "Mobile View"
	}

	def text(UiValidatorAlias ele) {
		if(ele.name != null) ele.name else "alias"
	}

	def text(UiBinding ele) {
		"Binding"
	}

	def text(UiBindingEndpointAlias ele) {
		if(ele.name != null) ele.name else "alias"
	}

	def text(UiBeanSlot ele) {
		if(ele.name != null) ele.name else "Beanslot"
	}

	def text(UiViewSet ele) {
		if(ele.name != null) ele.name else "ViewSet"
	}

	def text(UiFormLayout ele) {
		if(ele.name != null) ele.name else "Form"
	}

	def text(UiExposedAction ele) {
		if(ele.name != null) ele.name else "Action"
	}

	def text(UiVerticalLayout ele) {
		if(ele.name != null) ele.name else "Vertical Layout"
	}

	def text(UiHorizontalLayout ele) {
		if(ele.name != null) ele.name else "Horizontal Layout"
	}

	def text(UiVerticalComponentGroup ele) {
		if(ele.name != null) ele.name else "Vertical Group"
	}

	def text(UiHorizontalButtonGroup ele) {
		if(ele.name != null) ele.name else "Horizontal Group"
	}

	def text(UiTabSheet ele) {
		if(ele.name != null) ele.name else "Tabsheet"
	}

	def text(UiVisibilityProcessorAssignment ele) {
		if (ele.processor == null) {
			return "VisibilityProcessor"
		}
		if(ele.processor.name != null) ele.processor.name + "VisibilityProcessor" else "VisibilityProcessor"
	}

	def text(UiMobileTabSheet ele) {
		if(ele.name != null) ele.name else "Tabsheet"
	}

	def text(UiImage ele) {
		if(ele.name != null) ele.name else "Image"
	}

	def text(UiDialog ele) {
		if(ele.name != null) ele.name else "Dialog"
	}

	def text(UiMobileNavigationPage ele) {
		if(ele.name != null) ele.name else "Navigation Page"
	}

	def text(UiMobileNavigationRoot ele) {
		if(ele.name != null) ele.name else "Navigation Root"
	}

	def text(UiTextField ele) {
		if(ele.name != null) ele.name else "Textfield"
	}

	def text(UiTable ele) {
		if(ele.name != null) ele.name else "Table"
	}

	def text(UiSearchField ele) {
		if(ele.name != null) ele.name else "SearchField"
	}

	def text(UiComboBox ele) {
		if(ele.name != null) ele.name else "Checkbox"
	}

	def text(UiBeanReferenceField ele) {
		if(ele.name != null) ele.name else "Reference Field"
	}

	def text(UiButton ele) {
		if(ele.name != null) ele.name else "Button"
	}

	def text(UiMobileNavigationButton ele) {
		if(ele.name != null) ele.name else "Button"
	}

	def text(UiSwitch ele) {
		if(ele.name != null) ele.name else "Switch"
	}

	def text(UiColumnAssignments ele) {
		if(ele.name != null) ele.name else "column"
	}

	def text(UiColumn ele) {
		ele.name
	}

	def text(UiNumericField ele) {
		if(ele.name != null) ele.name else "Numberfield"
	}

	def text(UiCheckBox ele) {
		if(ele.name != null) ele.name else "Checkbox"
	}

	def text(UiMaxLengthValidator ele) {
		if(ele.name != null) ele.name else "Validator"
	}

	def text(UiMinLengthValidator ele) {
		if(ele.name != null) ele.name else "Validator"
	}

	def text(UiRegexpValidator ele) {
		if(ele.name != null) ele.name else "Validator"
	}

	def text(UiXbaseValidator ele) {
		if(ele.name != null) ele.name else "Validator"
	}

	def image(UiModel ele) {
		'UiModel.gif'
	}

	def image(UiOptionsGroup ele) {
		'UiOptionsGroup.gif'
	}

	def image(UiProgressBar ele) {
		'UiProgressBar.gif'
	}

	def image(UiIDEView ele) {
		'UiView.gif'
	}

	def image(UiMobileView ele) {
		'UiView.gif'
	}

	def image(UiVisibilityProcessorAssignment ele) {
		'UiVisibilityProcessor.gif'
	}

	def image(UiValidator ele) {
		'UiValidator.gif'
	}

	def image(UiValidatorAlias ele) {
		'UiValidatorAlias.gif'
	}

	def image(UiSearchDialog ele) {
		'UiSearchDialog.png'
	}

	def image(UiSearchField ele) {
		'UiSearchField.png'
	}

	def image(UiBinding ele) {
		'UiBinding.png'
	}

	def image(UiBindingEndpointAlias ele) {
		'UiBeanSlot.gif'
	}

	def image(UiBeanSlot ele) {
		'UiBeanSlot.gif'
	}

	def image(UiView ele) {
		'UiView.gif'
	}

	def image(UiViewSet ele) {
		'UiViewSet.png'
	}

	def image(UiFormLayout ele) {
		'UiFormLayout.gif'
	}

	def image(UiGridLayout ele) {
		'UiGridLayout.png'
	}

	def image(UiVerticalLayout ele) {
		'UiVerticalLayout.gif'
	}

	def image(UiI18nInfo ele) {
		'UiI18nInfo.gif'
	}

	def image(UiHorizontalLayout ele) {
		'UiHorizontalLayout.gif'
	}

	def image(UiVerticalComponentGroup ele) {
		'UiVerticalComponentGroup.gif'
	}

	def image(UiHorizontalButtonGroup ele) {
		'UiHorizontalButtonGroup.gif'
	}

	def image(UiTabSheet ele) {
		'UiTabSheet.png'
	}

	def image(UiTabAssignment ele) {
		'UiTabAssignment.gif'
	}

	def image(UiMobileTabSheet ele) {
		'UiMobileTabSheet.gif'
	}

	def image(UiMobileTabAssignment ele) {
		'UiMobileTabAssignment.gif'
	}

	def image(UiImage ele) {
		'UiImage.gif'
	}

	def image(UiDialog ele) {
		'UiDialog.png'
	}

	def image(UiMobileNavigationRoot ele) {
		'UiMobileNavigationRoot.gif'
	}

	def image(UiMobileNavigationPage ele) {
		'UiMobileNavigationPage.gif'
	}

	def image(UiTextField ele) {
		'UiTextField.gif'
	}

	def image(UiTextArea ele) {
		'UiTextArea.gif'
	}

	def image(UiTable ele) {
		'UiTable.gif'
	}

	def image(UiComboBox ele) {
		'UiCombobox.gif'
	}

	def image(UiBeanReferenceField ele) {
		'UiBeanReferenceField.gif'
	}

	def image(UiExposedAction ele) {
		'UiExposedAction.png'
	}

	def image(UiCommand ele) {
		'UiCommand.gif'
	}

	def image(UiButton ele) {
		'UiButton.gif'
	}

	def image(UiLabel ele) {
		'UiLabel.png'
	}

	def image(UiList ele) {
		'UiList.png'
	}

	def image(UiBrowser ele) {
		'UiBrowser.png'
	}

	def image(UiMobileNavigationButton ele) {
		'UiMobileNavigationButton.gif'
	}

	def image(UiSwitch ele) {
		'UiSwitch.gif'
	}

	def image(UiColumnAssignments ele) {
		'UiColumnAssignments.gif'
	}

	def image(UiColumn ele) {
		'UiColumn.gif'
	}

	def image(UiDateField ele) {
		'UiDateField.gif'
	}

	def image(UiDecimalField ele) {
		'UiDecimalField.gif'
	}

	def image(UiNumericField ele) {
		'UiNumericField.gif'
	}

	def image(UiCheckBox ele) {
		'UiCheckBox.gif'
	}

	def image(UiMaxLengthValidator ele) {
		'UiValidator.gif'
	}

	def image(UiMinLengthValidator ele) {
		'UiValidator.gif'
	}

	def image(UiRegexpValidator ele) {
		'UiValidator.gif'
	}

	def image(UiXbaseValidator ele) {
		'UiValidator.gif'
	}

}
