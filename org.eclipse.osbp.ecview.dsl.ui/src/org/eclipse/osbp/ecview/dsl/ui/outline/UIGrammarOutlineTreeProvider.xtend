/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.ui.outline

import org.eclipse.xtext.ui.editor.outline.IOutlineNode
import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanReferenceField
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanSlot
import org.eclipse.osbp.ecview.semantic.uimodel.UiBinding
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAlias
import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiList
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPage
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRoot
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileSearchPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel
import org.eclipse.osbp.ecview.semantic.uimodel.UiOptionsGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchPanel
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabSheet
import org.eclipse.osbp.ecview.semantic.uimodel.UiTable
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroup
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayout
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorAssignment
import org.eclipse.osbp.ecview.semantic.uimodel.UiXbaseValidator

/**
 * Customization of the default outline structure.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
class UIGrammarOutlineTreeProvider extends DefaultOutlineTreeProvider {

	def _isLeaf(UiBinding object) {
		return true;
	}

	def _isLeaf(UiBeanSlot object) {
		return true;
	}

	def _isLeaf(UiBindingEndpointAlias object) {
		return true;
	}

	def _isLeaf(UiBeanReferenceField object) {
		return true;
	}

	def _isLeaf(UiVisibilityProcessorAssignment object) {
		return true;
	}

	def _isLeaf(UiXbaseValidator object) {
		return true;
	}

	def void _createChildren(IOutlineNode parentNode, UiModel modelElement) {
		for (childElement : modelElement.roots) {
			createNode(parentNode, childElement);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiVerticalLayout modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiHorizontalLayout modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiMobileNavigationRoot modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiHorizontalButtonGroup modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiVerticalComponentGroup modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiGridLayout modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiFormLayout modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiMobileNavigationPage modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiMobileSearchPanel modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiSearchPanel modelElement) {
		for (childElement : modelElement.contents) {
			createNode(parentNode, childElement);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiTabSheet modelElement) {
		for (childElement : modelElement.tabs) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiMobileTabSheet modelElement) {
		for (childElement : modelElement.tabs) {
			createNode(parentNode, childElement.element);
		}
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiTable modelElement) {
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiOptionsGroup modelElement) {
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiComboBox modelElement) {
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiList modelElement) {
		for (binding : modelElement.bindings) {
			createNode(parentNode, binding);
		}
	}

	def void _createChildren(IOutlineNode parentNode, UiBindingEndpointAlias modelElement) {
		//		for(binding : modelElement.bindings){
		//			createNode(parentNode, binding);
		//		}
	}

	def void _createChildren(IOutlineNode parentNode, UiBinding modelElement) {
	}

}
