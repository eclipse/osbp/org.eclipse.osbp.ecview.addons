/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.ui;

import org.eclipse.osbp.ecview.dsl.ui.hover.UiGrammarHoverDocumentationProvider;
import org.eclipse.osbp.ide.core.api.i18n.II18nRegistry;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderProvider;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.IJdtTypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.IJdtTypeLoaderProvider;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.impl.JdtTypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.impl.JdtTypeLoaderProvider;
import org.eclipse.osbp.xtext.oxtype.imports.IUnresolvedEObjectResolver;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXTypeReplacingAppendable;
import org.eclipse.osbp.xtext.oxtype.ui.imports.InteractiveUnresolvedEClassResolver;
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.CustomJavaTypeQuickfixes;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.xbase.ui.contentassist.ReplacingAppendable;
import org.eclipse.xtext.xbase.ui.quickfix.JavaTypeQuickfixes;

import com.google.inject.Binder;
import com.google.inject.Singleton;

/**
 * Use this class to register components to be used within the IDE.
 */
@SuppressWarnings("restriction")
public class UIGrammarUiModule extends
		org.eclipse.osbp.ecview.dsl.ui.AbstractUIGrammarUiModule {
	public UIGrammarUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}

	public void configureITypeLoaderFactory(Binder binder) {
		binder.bind(ITypeLoaderFactory.class).to(JdtTypeLoaderFactory.class)
				.in(Singleton.class);
		binder.bind(IJdtTypeLoaderFactory.class).to(JdtTypeLoaderFactory.class)
				.in(Singleton.class);
	}

	public void configureITypeLoaderProvider(Binder binder) {
		binder.bind(ITypeLoaderProvider.class).to(JdtTypeLoaderProvider.class)
				.in(Singleton.class);
		binder.bind(IJdtTypeLoaderProvider.class)
				.to(JdtTypeLoaderProvider.class).in(Singleton.class);
	}

	public Class<? extends org.eclipse.xtext.ui.editor.hover.html.IEObjectHoverDocumentationProvider> bindIEObjectHoverDocumentationProvider() {
		return UiGrammarHoverDocumentationProvider.class;
	}

	public com.google.inject.Provider<II18nRegistry> provideII18nRegistry() {
		return org.eclipse.osbp.ide.core.ui.shared.Access.getII18nRegistry();
	}

	public Class<? extends ReplacingAppendable.Factory> bindReplacingAppendable$Factory() {
		return OXTypeReplacingAppendable.Factory.class;
	}

	public Class<? extends IUnresolvedEObjectResolver> bindIUnresolvedEObjectResolver() {
		return InteractiveUnresolvedEClassResolver.class;
	}

	public Class<? extends JavaTypeQuickfixes> bindJavaTypeQuickfixes() {
		return CustomJavaTypeQuickfixes.class;
	}

}
