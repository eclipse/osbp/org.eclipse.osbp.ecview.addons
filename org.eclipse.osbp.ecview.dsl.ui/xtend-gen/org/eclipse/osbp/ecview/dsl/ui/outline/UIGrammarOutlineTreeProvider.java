/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.dsl.ui.outline;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanReferenceField;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBeanSlot;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBinding;
import org.eclipse.osbp.ecview.semantic.uimodel.UiBindingEndpointAlias;
import org.eclipse.osbp.ecview.semantic.uimodel.UiComboBox;
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayout;
import org.eclipse.osbp.ecview.semantic.uimodel.UiFormLayoutAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayout;
import org.eclipse.osbp.ecview.semantic.uimodel.UiGridLayoutAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroup;
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalButtonGroupAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayout;
import org.eclipse.osbp.ecview.semantic.uimodel.UiHorizontalLayoutAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiList;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPage;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationPageAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRoot;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileNavigationRootAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileSearchPanel;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiMobileTabSheet;
import org.eclipse.osbp.ecview.semantic.uimodel.UiModel;
import org.eclipse.osbp.ecview.semantic.uimodel.UiOptionsGroup;
import org.eclipse.osbp.ecview.semantic.uimodel.UiRootElements;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchField;
import org.eclipse.osbp.ecview.semantic.uimodel.UiSearchPanel;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTabSheet;
import org.eclipse.osbp.ecview.semantic.uimodel.UiTable;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroup;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalComponentGroupAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayout;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVerticalLayoutAssigment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiVisibilityProcessorAssignment;
import org.eclipse.osbp.ecview.semantic.uimodel.UiXbaseValidator;
import org.eclipse.xtext.ui.editor.outline.IOutlineNode;
import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider;

/**
 * Customization of the default outline structure.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
@SuppressWarnings("all")
public class UIGrammarOutlineTreeProvider extends DefaultOutlineTreeProvider {
  public boolean _isLeaf(final UiBinding object) {
    return true;
  }
  
  public boolean _isLeaf(final UiBeanSlot object) {
    return true;
  }
  
  public boolean _isLeaf(final UiBindingEndpointAlias object) {
    return true;
  }
  
  public boolean _isLeaf(final UiBeanReferenceField object) {
    return true;
  }
  
  public boolean _isLeaf(final UiVisibilityProcessorAssignment object) {
    return true;
  }
  
  public boolean _isLeaf(final UiXbaseValidator object) {
    return true;
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiModel modelElement) {
    EList<UiRootElements> _roots = modelElement.getRoots();
    for (final UiRootElements childElement : _roots) {
      this.createNode(parentNode, childElement);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiVerticalLayout modelElement) {
    EList<UiVerticalLayoutAssigment> _contents = modelElement.getContents();
    for (final UiVerticalLayoutAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiHorizontalLayout modelElement) {
    EList<UiHorizontalLayoutAssigment> _contents = modelElement.getContents();
    for (final UiHorizontalLayoutAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiMobileNavigationRoot modelElement) {
    EList<UiMobileNavigationRootAssigment> _contents = modelElement.getContents();
    for (final UiMobileNavigationRootAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiHorizontalButtonGroup modelElement) {
    EList<UiHorizontalButtonGroupAssigment> _contents = modelElement.getContents();
    for (final UiHorizontalButtonGroupAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiVerticalComponentGroup modelElement) {
    EList<UiVerticalComponentGroupAssigment> _contents = modelElement.getContents();
    for (final UiVerticalComponentGroupAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiGridLayout modelElement) {
    EList<UiGridLayoutAssigment> _contents = modelElement.getContents();
    for (final UiGridLayoutAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiFormLayout modelElement) {
    EList<UiFormLayoutAssigment> _contents = modelElement.getContents();
    for (final UiFormLayoutAssigment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiMobileNavigationPage modelElement) {
    EList<UiMobileNavigationPageAssignment> _contents = modelElement.getContents();
    for (final UiMobileNavigationPageAssignment childElement : _contents) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiMobileSearchPanel modelElement) {
    EList<UiSearchField> _contents = modelElement.getContents();
    for (final UiSearchField childElement : _contents) {
      this.createNode(parentNode, childElement);
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiSearchPanel modelElement) {
    EList<UiSearchField> _contents = modelElement.getContents();
    for (final UiSearchField childElement : _contents) {
      this.createNode(parentNode, childElement);
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiTabSheet modelElement) {
    EList<UiTabAssignment> _tabs = modelElement.getTabs();
    for (final UiTabAssignment childElement : _tabs) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiMobileTabSheet modelElement) {
    EList<UiMobileTabAssignment> _tabs = modelElement.getTabs();
    for (final UiMobileTabAssignment childElement : _tabs) {
      this.createNode(parentNode, childElement.getElement());
    }
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiTable modelElement) {
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiOptionsGroup modelElement) {
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiComboBox modelElement) {
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiList modelElement) {
    EList<UiBinding> _bindings = modelElement.getBindings();
    for (final UiBinding binding : _bindings) {
      this.createNode(parentNode, binding);
    }
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiBindingEndpointAlias modelElement) {
  }
  
  public void _createChildren(final IOutlineNode parentNode, final UiBinding modelElement) {
  }
}
