/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uisemantics.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.semantic.uisemantics.UxEndpointDef;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ux Endpoint Def</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UxEndpointDefTest extends TestCase {

	/**
	 * The fixture for this Ux Endpoint Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UxEndpointDef fixture = null;

	/**
	 * Constructs a new Ux Endpoint Def test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UxEndpointDefTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ux Endpoint Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UxEndpointDef fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ux Endpoint Def test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UxEndpointDef getFixture() {
		return fixture;
	}

} //UxEndpointDefTest
