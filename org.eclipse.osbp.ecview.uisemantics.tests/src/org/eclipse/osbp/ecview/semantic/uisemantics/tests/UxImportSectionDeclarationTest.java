/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uisemantics.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsFactory;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxImportSectionDeclaration;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ux Import Section Declaration</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UxImportSectionDeclarationTest extends TestCase {

	/**
	 * The fixture for this Ux Import Section Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UxImportSectionDeclaration fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UxImportSectionDeclarationTest.class);
	}

	/**
	 * Constructs a new Ux Import Section Declaration test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UxImportSectionDeclarationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Ux Import Section Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UxImportSectionDeclaration fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Ux Import Section Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UxImportSectionDeclaration getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiSemanticsFactory.eINSTANCE.createUxImportSectionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UxImportSectionDeclarationTest
