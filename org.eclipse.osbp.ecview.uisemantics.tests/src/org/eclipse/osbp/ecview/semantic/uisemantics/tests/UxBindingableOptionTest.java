/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uisemantics.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsFactory;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxBindingableOption;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ux Bindingable Option</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UxBindingableOptionTest extends UxEndpointDefTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UxBindingableOptionTest.class);
	}

	/**
	 * Constructs a new Ux Bindingable Option test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UxBindingableOptionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ux Bindingable Option test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UxBindingableOption getFixture() {
		return (UxBindingableOption)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UiSemanticsFactory.eINSTANCE.createUxBindingableOption());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UxBindingableOptionTest
