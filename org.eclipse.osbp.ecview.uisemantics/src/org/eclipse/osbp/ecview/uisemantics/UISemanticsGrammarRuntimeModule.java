/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.uisemantics;

import org.eclipse.osbp.ecview.uisemantics.formatting.UISemanticsGrammarFormatter;
import org.eclipse.osbp.ecview.uisemantics.linking.UiSemanticsLinkingService;
import org.eclipse.osbp.ecview.uisemantics.naming.QualifiedNameProvider;
import org.eclipse.osbp.ecview.uisemantics.scoping.ScopeProvider;
import org.eclipse.osbp.ecview.uisemantics.scoping.UiImportedNamespaceAwareLocalScopeProvider;
import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.formatting.IFormatter;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.resource.BatchLinkableResourceStorageFacade;
import org.eclipse.xtext.xbase.scoping.batch.IBatchScopeProvider;

import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
public class UISemanticsGrammarRuntimeModule
		extends org.eclipse.osbp.ecview.uisemantics.AbstractUISemanticsGrammarRuntimeModule {

	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return BatchLinkableResourceStorageFacade.class;
	}
	
	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}

	public Class<? extends IFormatter> bindIFormatter() {
		return UISemanticsGrammarFormatter.class;
	}
	
	public Class<? extends org.eclipse.xtext.naming.IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return QualifiedNameProvider.class;
	}

	@Override
	public Class<? extends IScopeProvider> bindIScopeProvider() {
		return ScopeProvider.class;
	}

	public Class<? extends IBatchScopeProvider> bindIBatchScopeProvider() {
		return ScopeProvider.class;
	}

	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class)
				.annotatedWith(Names.named("org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider.delegate"))
				.to(UiImportedNamespaceAwareLocalScopeProvider.class);
	}

	public Class<? extends ILinkingService> bindILinkingService() {
		return UiSemanticsLinkingService.class;
	}

}
