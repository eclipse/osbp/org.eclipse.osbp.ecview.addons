/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.uisemantics.naming;

import java.util.UUID;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.scoping.XbaseQualifiedNameProvider;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxAction;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxAvailableBindings;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxEPackageImport;

import com.google.inject.Inject;

@SuppressWarnings("restriction")
public class QualifiedNameProvider extends XbaseQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter nameConverter;

	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj instanceof EPackage) {
			EPackage pkg = (EPackage) obj;
			return QualifiedName.create(pkg.getNsURI());
		} else if (obj instanceof UxEPackageImport) {
			UxEPackageImport pkg = (UxEPackageImport) obj;
			if (pkg.getAlias() != null && !pkg.getAlias().equals("")) {
				return QualifiedName.create(pkg.getAlias());
			} else {
				return QualifiedName.EMPTY;
			}
		} else if (obj instanceof UxAction) {
			return nameConverter.toQualifiedName(((UxAction) obj).getName());
		} else if (obj instanceof UxAvailableBindings) {
			// return a random value to ensure this element will be exported by the index
			return super.getFullyQualifiedName(obj.eContainer()).append("bindings");
		}

		return super.getFullyQualifiedName(obj);
	}

}
