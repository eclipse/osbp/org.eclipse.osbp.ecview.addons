/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uisemantics.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsPackage;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxAvailableBindings;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxBindingableOption;
import org.eclipse.osbp.ecview.semantic.uisemantics.UxElementDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ux Available Bindings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uisemantics.impl.UxAvailableBindingsImpl#getSuperElement <em>Super Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uisemantics.impl.UxAvailableBindingsImpl#getBindings <em>Bindings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UxAvailableBindingsImpl extends MinimalEObjectImpl.Container implements UxAvailableBindings {
	/**
	 * The cached value of the '{@link #getSuperElement() <em>Super Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperElement()
	 * @generated
	 * @ordered
	 */
	protected UxElementDefinition superElement;
	/**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<UxBindingableOption> bindings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UxAvailableBindingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiSemanticsPackage.Literals.UX_AVAILABLE_BINDINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UxElementDefinition getSuperElement() {
		if (superElement != null && superElement.eIsProxy()) {
			InternalEObject oldSuperElement = (InternalEObject)superElement;
			superElement = (UxElementDefinition)eResolveProxy(oldSuperElement);
			if (superElement != oldSuperElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT, oldSuperElement, superElement));
			}
		}
		return superElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UxElementDefinition basicGetSuperElement() {
		return superElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperElement(UxElementDefinition newSuperElement) {
		UxElementDefinition oldSuperElement = superElement;
		superElement = newSuperElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT, oldSuperElement, superElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UxBindingableOption> getBindings() {
		if (bindings == null) {
			bindings = new EObjectContainmentEList<UxBindingableOption>(UxBindingableOption.class, this, UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS:
				return ((InternalEList<?>)getBindings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT:
				if (resolve) return getSuperElement();
				return basicGetSuperElement();
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS:
				return getBindings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT:
				setSuperElement((UxElementDefinition)newValue);
				return;
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends UxBindingableOption>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT:
				setSuperElement((UxElementDefinition)null);
				return;
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS:
				getBindings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__SUPER_ELEMENT:
				return superElement != null;
			case UiSemanticsPackage.UX_AVAILABLE_BINDINGS__BINDINGS:
				return bindings != null && !bindings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UxAvailableBindingsImpl
