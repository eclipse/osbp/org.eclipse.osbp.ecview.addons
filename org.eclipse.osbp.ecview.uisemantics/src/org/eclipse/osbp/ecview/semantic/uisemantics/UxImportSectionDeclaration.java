/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.semantic.uisemantics;

import org.eclipse.xtext.xtype.XImportDeclaration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ux Import Section Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.semantic.uisemantics.UxImportSectionDeclaration#getImportedEPackage <em>Imported EPackage</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsPackage#getUxImportSectionDeclaration()
 * @model
 * @generated
 */
public interface UxImportSectionDeclaration extends XImportDeclaration {
	/**
	 * Returns the value of the '<em><b>Imported EPackage</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported EPackage</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported EPackage</em>' containment reference.
	 * @see #setImportedEPackage(UxEPackageImport)
	 * @see org.eclipse.osbp.ecview.semantic.uisemantics.UiSemanticsPackage#getUxImportSectionDeclaration_ImportedEPackage()
	 * @model containment="true"
	 * @generated
	 */
	UxEPackageImport getImportedEPackage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.semantic.uisemantics.UxImportSectionDeclaration#getImportedEPackage <em>Imported EPackage</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imported EPackage</em>' containment reference.
	 * @see #getImportedEPackage()
	 * @generated
	 */
	void setImportedEPackage(UxEPackageImport value);

} // UxImportSectionDeclaration
