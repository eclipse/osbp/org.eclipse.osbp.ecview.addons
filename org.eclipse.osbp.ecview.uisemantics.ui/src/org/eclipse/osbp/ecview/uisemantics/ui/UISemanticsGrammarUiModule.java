/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.uisemantics.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.osbp.ecview.uisemantics.ui.hover.UiSemanticGrammarHoverDocumentationProvider;

/**
 * Use this class to register components to be used within the IDE.
 */
public class UISemanticsGrammarUiModule extends org.eclipse.osbp.ecview.uisemantics.ui.AbstractUISemanticsGrammarUiModule {
	public UISemanticsGrammarUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	public Class<? extends org.eclipse.xtext.ui.editor.hover.html.IEObjectHoverDocumentationProvider> bindIEObjectHoverDocumentationProvider() {
		return UiSemanticGrammarHoverDocumentationProvider.class;
	}
}
