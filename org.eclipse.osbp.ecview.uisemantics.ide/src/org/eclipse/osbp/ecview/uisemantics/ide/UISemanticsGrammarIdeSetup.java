/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.uisemantics.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.osbp.ecview.uisemantics.UISemanticsGrammarRuntimeModule;
import org.eclipse.osbp.ecview.uisemantics.UISemanticsGrammarStandaloneSetup;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class UISemanticsGrammarIdeSetup extends UISemanticsGrammarStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new UISemanticsGrammarRuntimeModule(), new UISemanticsGrammarIdeModule()));
	}
	
}
